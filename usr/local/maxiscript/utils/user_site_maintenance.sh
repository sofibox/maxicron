#!/bin/bash

# Author: Arafat Ali (arafat@maxibi.com)
# This script was created to put specific user website into maintenance mode when running a system backup using Directadmin
# Create web_config.ini [optional] to specify type of website eg. wordpress or prestashop or magento and other sites that support .htaccess
# If you don't create web_config.ini, the script will create a generic config template.
# This is very useful when doing a site backup and put all websites in a maintenance mode
# so users don't disturb database that can cause database corrupt during system backup
# usage ./u_site_maintenance debug username 0  #0=maintenance mode, 1=live mode
# This command will put all domains including the subfolder applications of a specific user into maintenance or live mode.
# Hardcoded. More works need to be done.

script_name=$(basename -- "$0")
script_path=$(dirname $(realpath -s $0))
DATE_BIN=`/usr/bin/date +%s`
MYEMAIL="webmaster@sofibox.com"
ECHO_BIN="/usr/bin/echo"
BACKUP_LOG_PATH="$script_path/log"
REPORT_FILE="$BACKUP_LOG_PATH/$script_name.$DATE_BIN.$RANDOM.log"
MYHOSTNAME=`/bin/hostname`
MAIL_BIN="/usr/local/bin/mail"
MAINTENANCE_MODE="N/A"
WARNING_STATUS="OK"
web_user=$1
maintenance_status=$2
maintenance_time=$3
running_mode=$4
# Can declare many subfolders here to check - This is hardcoded, will work on this.
# normally you declare common subfolder that contains another type of web application to be put in maintenance mode such as '/wordpress'
# don't worry, folders that dont exist for that users will be skip checking. So, only this subfolder declared here for each user will be put in maintenance mode
declare -a web_subfolders=('' '/www' '/forums' '/code' '/test' '/wifi' '/dev');
# Directadmin defined domain list for specific users:

web_domains=$(cat /usr/local/directadmin/data/users/$web_user/domains.list)
if [ -z "$web_domains" ]; then # If the domain list is empty  (If don't specify this function, it will also break in $check_web_subfolder) because the array domains.list is empty
    echo "[$script_name | info]: Warning, the user [$web_user] does not have any domain created" | tee -a $REPORT_FILE
    #exit 1 # Don't exit better skip because this one will break DA backup function So, we depend on check_web_subfolder
fi
for web_domain in $web_domains
do
    for web_subfolder in "${web_subfolders[@]}"
    do
        # Skip subfolder that does not exist from the declaration # This also will skip if users don't have domain created
        # No domain created meaning there is no subfolder inside domains folder
        check_web_subfolder="/home/$web_user/domains/$web_domain/public_html/${web_subfolder}"
        if ! [ -d "$check_web_subfolder" ]; then
            continue
        fi
        if [ -z "$web_subfolder" ]; then
                                web_subfolder_info="/"
                        else
                                web_subfolder_info="$web_subfolder"
                fi
        # Define web_config.ini location
        web_config="/home/$web_user/domains/$web_domain/public_html${web_subfolder}/web_config.ini"
        # If web_config.ini is found, then we read the config file
        if [ -f "$web_config" ]; then
            #echo "[$script_name | info]: [web_config.ini] found at $web_config. Loading web_config setting ..." | tee -a $REPORT_FILE
            source $web_config
            # Variables of web_config can be obtain here
        else
            echo "[$script_name | info]: Warning, [web_config.ini] does not exist in $web_config" | tee -a $REPORT_FILE
            {
                echo "web_type='general'"
                echo "admin_path=''"
            } >> $web_config
            chown $web_user:$web_user $web_config
            chmod 644 $web_config
            echo "[$script_name | info]: A new [web_config.ini] file has been created in $web_config. Loading web_config setting ..." | tee -a $REPORT_FILE
            source $web_config
            # Variables of web_config can be obtain here (blank)
        fi
        # Define maintenance page
        maintenance_htmlpage="/home/$web_user/domains/$web_domain/public_html${web_subfolder}/maintenance.html"
        # If maintenance_time is set display it on the maintenance page
        if [ -n "$maintenance_time" ]; then
            auto_approximate_notice="<p>The approximate time to finish this maintenance is less than $maintenance_time second(s)</p>"
        fi
        # Directadmin location for users directory pattern
        maintenance_htaccess_root="/home/$web_user/domains/$web_domain/public_html${web_subfolder}/.htaccess"
        # if the htaccess file exist, we look for the keyword MAXIRSYNC_BACKUP_CRON
        if [[ -f "$maintenance_htaccess_root" ]]; then
            maintenance_mode_root="$(head -1 "${maintenance_htaccess_root}" | grep 'MAXIRSYNC_BACKUP_CRON')"
        else
            echo "[$script_name | info]: Warning, the main .htaccess file does not exist in the location speficied in [$maintenance_htaccess_root]" | tee -a $REPORT_FILE
            #WARNING_STATUS="WARNING" # No need to warn because I create htaccess for you
            echo "#" > "${maintenance_htaccess_root}" # Create a blank .htaccess
            chown $web_user:$web_user "${maintenance_htaccess_root}"
            chmod 644 "${maintenance_htaccess_root}"
            echo "[$script_name | info]: OK, a blank .htaccess file has been created in ${maintenance_htaccess_root}" | tee -a $REPORT_FILE
            #$MAIL_BIN -s "[$WARNING_STATUS: $web_domain] Maintenance Operation Report" $MYEMAIL < $REPORT_FILE
        fi

        if [ "$maintenance_status" == "0" ]; then
            echo "[$script_name | info]: Going into maintenance mode for user [$web_user] for website [$web_domain] inside a folder [$web_subfolder_info]" | tee -a $REPORT_FILE
            #echo "[$script_name | info]: Website type for $web_domain$web_subfolder is: $web_type" | tee -a $REPORT_FILE
            if [ -z "$maintenance_mode_root" ]; then # If the htaccess doesn't have maintenance content (empty grep) we will set maintenance mode
                cat << EOT > "$maintenance_htmlpage"
<!DOCTYPE html>
<html>
    <!--
    This is an auto generated maintenance page by maxicron script created by Arafat Ali
    -->
    <head>
        <meta http-equiv="Refresh" content="5; url='http://$web_domain'" />
        <title>$web_domain-Website Maintenance</title>
        <style>
            body { text-align: center; padding: 150px; }
            h1 { font-size: 50px; }
            body { font: 20px Helvetica, sans-serif; color: #333; }
            article { display: block; text-align: left; width: 650px; margin: 0 auto; }
            a { color: #dc8100; text-decoration: none; }
            a:hover { color: #333; text-decoration: none; }
        </style>
    </head>
<body>
<article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
    <p>Sorry for the inconvenience but we&rsquo;re performing site maintenance at the moment. If you need to you can always <a href="mailto:$MYEMAIL">contact us</a>, otherwise <strong>$web_domain</strong> will be online shortly!</p>
    $auto_approximate_notice
    <p>&mdash; The Team</p>
    </div>
</article>
</body>
</html>
EOT
chmod 644 "${maintenance_htmlpage}"
chown "${web_user}":"${web_user}" "${maintenance_htmlpage}"
# first rename the .htaccess as .htaccess_x
mv "${maintenance_htaccess_root}" "${maintenance_htaccess_root}_x"
# Create new htaccess file
touch "${maintenance_htaccess_root}"
# then write some maintenance contents
{
echo "# AUTO GENERATED BY MAXIRSYNC_BACKUP_CRON"
echo "# This htaccess putting website $web_domain into maintenance mode"
echo "RewriteEngine on"
echo "RewriteCond %{REQUEST_URI} !/maintenance.html$ [NC]"
echo "RewriteRule .* /maintenance.html [R=302,L]"
} >> "${maintenance_htaccess_root}"
# make sure the file pemrission are correct
chmod 644 "${maintenance_htaccess_root}"
chown "${web_user}":"${web_user}" "${maintenance_htaccess_root}"
# pause a little bit
sleep 1

if [ "${web_type}" == "prestashop" ]; then
    # DO PRESTASHOP THING
    # The default maintenance mode for prestashop backend doesn't block admin login. So when doing backup, somebody in the admin side can still change database.
    # In order to prevent that, we create a new .htaccess in that admin path defined by webconfig.ini
    # This issue was disscussed here: https://www.prestashop.com/forums/topic/1029258-any-alternative-ways-to-put-prestashop-into-maintenace-mode/?tab=comments#comment-3256264
    # for prestashop maintenance file
    ps_maintenance_htaccess_admin="/home/$web_user/domains/$web_domain/public_html$web_subfolder$admin_path/.htaccess"
    if [[ -f "$ps_maintenance_htaccess_admin" ]]; then
        ps_maintenance_mode_admin="$(head -1 ${ps_maintenance_htaccess_admin} | grep 'MAXIRSYNC_BACKUP_CRON')"
    else
        echo "[$script_name | info]: The prestashop admin .htaccess file does not exist in the location specified in [$ps_maintenance_htaccess_admin]" | tee -a $REPORT_FILE
        WARNING_STATUS="WARNING"
        echo "#" > $ps_maintenance_htaccess_admin
        chown $web_user:$web_user $ps_maintenance_htaccess_admin
        chmod 644 $ps_maintenance_htaccess_admin
        echo "[$script_name | info]: A blank .htaccess file has been created in $ps_maintenance_htaccess_admin" | tee -a $REPORT_FILE
        #$MAIL_BIN -s "[$WARNING_STATUS: $web_domain] Maintenance Operation Report" $MYEMAIL < $REPORT_FILE
        #exit 1
    fi
    #######################################
    # copy current htccess inside admin_dash into .htaccess_y
    if [ -z "$ps_maintenance_mode_admin" ]; then # If the htaccess doesn't have maintenance content we will set maintenance mode
        mv "${ps_maintenance_htaccess_admin}" "${ps_maintenance_htaccess_admin}_y"
        # copy the same .htaccess including permission file from root to admin_dash (overwrite)
        cp -p "${maintenance_htaccess_root}" "${ps_maintenance_htaccess_admin}"
        # Setting permission to the original state;
        chmod 644 $ps_maintenance_htaccess_admin
        chown $web_user:$web_user $ps_maintenance_htaccess_admin
    fi
elif [ "$web_type" == "wordpress" ]; then
    # DO EXTRA WORDPRESS THING
    :
elif [ "$web_type" == "joomla" ]; then
    :
elif [ "$web_type" == "drupal" ]; then
    :
elif [ "$web_type" == "magento" ]; then
    :
elif [ "$web_type" == "opencart" ]; then
    :
fi
echo "[$script_name | info]: OK, the website of $web_domain inside $web_subfolder is now under maintenance mode" | tee -a $REPORT_FILE
MAINTENANCE_MODE="MAINTENANCE"
    else
        echo "[$script_name | info]: The website of $web_user on $web_domain [at $web_subfolder_info] is already in maintenance mode" | tee -a $REPORT_FILE
        MAINTENANCE_MODE="MAINTENANCE"
    fi
else # maintenance status = 1 (TURN ON WEBSITE)
##
    echo "[$script_name | info]: Going to live mode for user $web_user on $web_domain [at $web_subfolder_info]" | tee -a $REPORT_FILE
    if [ -z "$maintenance_mode_root" ]; then # If the htaccess doesn't have maintenance content we will set maintenance mode
        echo "[$script_name | info]: The website of user $web_user on $web_domain [at $web_subfolder_info] is not in maintenance mode" | tee -a $REPORT_FILE
        MAINTENANCE_MODE="LIVE"
    else
        # copy the backup .htaccess_x into .htaccess (and overwrite)
        cp -f -p "${maintenance_htaccess_root}_x" "${maintenance_htaccess_root}"
        chmod 644 $maintenance_htaccess_root
        chown $web_user:$web_user $maintenance_htaccess_root
        if [ "$web_type" == "prestashop" ]; then
            # PS
            # copy the admin_path .htaccess_y backup into .htaccess inside that admin_dash folder (and overwrite)
                   ps_maintenance_htaccess_admin="/home/$web_user/domains/$web_domain/public_html$web_subfolder$admin_path/.htaccess"
            #echo "ps_maintenance_htaccess_admin: ${ps_maintenance_htaccess_admin}_y"
            if [ -f "${ps_maintenance_htaccess_admin}_y" ]; then
                cp -f -p "${ps_maintenance_htaccess_admin}_y" "${ps_maintenance_htaccess_admin}"
                chmod 644 $ps_maintenance_htaccess_admin
                chown $web_user:$web_user $ps_maintenance_htaccess_admin
            else
                echo "[$script_name | info]: Warning the backup file of .htaccess_y at .. domains/$web_domain/public_html$web_subfolder$admin_path/ cannot be found" | tee -a $REPORT_FILE
                        exit 1
            fi
        elif [ "$web_type" == "wordpress" ]; then
            # WP
            :
        elif [ "$web_type" == "joomla" ]; then
                :
        elif [ "$web_type" == "drupal" ]; then
                :
        elif [ "$web_type" == "magento" ]; then
                    :
        elif [ "$web_type" == "opencart" ]; then
            :
        fi
        echo "[$script_name | info]: OK, the website for user $web_user on $web_domain [at $web_subfolder_info] is now under live mode" | tee -a $REPORT_FILE
        MAINTENANCE_MODE="LIVE"
    fi
 ##
fi

if [ -z "$running_mode" ]; then
    running_mode="manual"
fi
     echo "[$script_name | info]: Status: ${WARNING_STATUS}" | tee -a $REPORT_FILE
if [ WARNING_STATUS == "WARNING" ]; then
    $MAIL_BIN -s "[$web_domain at $web_subfolder_info is in $MAINTENANCE_MODE mode | $running_mode] Maintenance Operation Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
fi
done
done
