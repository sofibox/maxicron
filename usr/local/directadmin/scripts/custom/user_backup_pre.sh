#!/bin/bash

# Directadmin user_backup_pre.sh
# Author - Arafat Ali

script_path=$(dirname $(realpath -s $0))
script_name=$(basename -- "$0")
MAIL_BIN="/usr/local/bin/mail"
REPORT_DATE=`/usr/bin/date +%s`
REPORT_PATH="$script_path/log"
REPORT_FILE="$REPORT_PATH/$script_name-$REPORT_DATE.$RANDOM.log"
MAIL_BIN="/usr/local/bin/mail"
MYEMAIL="webmaster@sofibox.com"
WARNING_STATUS="OK"
MAINTENANCE_SCRIPT="/usr/local/maxiscript/utils/maxinance/u_site_maintenance"
mkdir -p $REPORT_PATH

# maintenance on (live mode off) for this users:
echo "[$script_name | info]: Turning on maintenance mode for user $username " | tee -a $REPORT_FILE
$MAINTENANCE_SCRIPT $username 0
# Only email if want to see realtime log
#$MAIL_BIN -s "[$script_name, $username]: Maintenance Mode" $MYEMAIL < $REPORT_FILE