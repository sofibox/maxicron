#!/bin/bash

# Directadmin user_backup_post.sh
# Author - Arafat Ali

script_path=$(dirname $(realpath -s $0))
script_name=$(basename -- "$0")
MAIL_BIN="/usr/local/bin/mail"
REPORT_DATE=`/usr/bin/date +%s`
REPORT_PATH="$script_path/log"
REPORT_FILE="$REPORT_PATH/$script_name-$REPORT_DATE.$RANDOM.log"
MAIL_BIN="/usr/local/bin/mail"
MYEMAIL="webmaster@sofibox.com"
WARNING_STATUS="OK"

# maintenance off

if [ "$username" == "user1" ]; then
        mkdir -p $script_path/log
        find $REPORT_PATH -name "*.log" -mtime +2 -exec rm {} \;
        echo "[$script_name | info]: Turning off server maintenance mode for user $username:" | tee -a $REPORT_FILE
        echo "[$script_name | info]: Temporarily disable website lozira.com (website will never put in live mode):" | tee -a $REPORT_FILE
        LOZDBUSER="user"
        LOZDBPASS="private"
        LOZDBNAME="privatename"
        # For lozira.com (prestashop)
        # disable shop from back office
        #mysql -u $LOZDBUSER -p$LOZDBPASS -D $LOZDBNAME -e "UPDATE _x1x7configuration SET value = 1 WHERE _x1x7configuration.id_configuration = 28;"

        #if [ $? -eq 0 ]; then
        #        echo "[$script_name | info]: Ok, the website of lozira.com is now under live  mode" | tee -a $REPORT_FILE
        #else
        #        echo "[$script_name | info]: Warning, unable to put website of lozira.com in live mode" | tee -a $REPORT_FILE
        #        WARNING_STATUS="WARNING"
        #fi
        echo "[$script_name | info]: Status: [$WARNING_STATUS]" | tee -a $REPORT_FILE
        echo "[$script_name | info]: Email notification is set to [$MYEMAIL]" | tee -a $REPORT_FILE
        echo "===================================================================================="
        $MAIL_BIN -s "[$script_name, $username | $WARNING_STATUS]: [LIVE MODE] Maintenance Operation Report" $MYEMAIL < $REPORT_FILE

elif [ "$username" == "user2" ]; then
        mkdir -p $script_path/log
        find $REPORT_PATH -name "*.log" -mtime +2 -exec rm {} \;
        if [ -f /home/private/domains/domain.com/public_html/www/.maintenance ]; then
                mv /home/private0/domains/domain.com/public_html/www/.maintenance /home/private/domains/domain.com/public_html/www/.maintenance_disable
                if [ $? -eq 0 ]
                then
                        echo "[$script_name | info]: Ok, the website of domain.com is now under live mode" | tee -a $REPORT_FILE
                else
                        echo "[$script_name | info]: Warning, unable to put website of domain.com into live mode" | tee -a $REPORT_FILE
                        WARNING_STATUS="WARNING"
                fi
        fi
        #elif [ "$username" == "codegix00" ]; then
        #       echo "later"

        echo "[$script_name | info]: Status: [$WARNING_STATUS]" | tee -a $REPORT_FILE
        echo "[$script_name | info]: Email notification is set to [$MYEMAIL]" | tee -a $REPORT_FILE
        echo "===================================================================================="
        $MAIL_BIN -s "[$script_name, $username | $WARNING_STATUS]: [LIVE MODE] Maintenance Operation Report" $MYEMAIL < $REPORT_FILE