#!/bin/bash

# Directadmin user_backup_success.sh
# Author - Arafat Ali
#cd /home/$username/domains/
# This script will check the backup file for corruption that was triggered from DA backup
# It also will auto encrypt the backup file and upload only the encrypted backup to cloud

script_path=$(dirname $(realpath -s $0))
script_name=$(basename -- "$0")
MAIL_BIN="/usr/local/bin/mail"
REPORT_DATE=`/usr/bin/date +%s`
REPORT_FILE="$script_path/log/$script_name-$DATE_DATE.log"
MAIL_BIN="/usr/local/bin/mail"
MYEMAIL="webmaster@sofibox.com"
ENC_PASS="/root/.enc_password"
ENCRYPT_FILE="/usr/local/maxiscript/utils/encrypt_file.sh"
#mkdir -p $script_path/log
#env | grep -v pass > $REPORT_FILE
#$MAIL_BIN -s "[$script_name]: $username Report" $MYEMAIL < $REPORT_FILE


if ! [ "$local_path" == "/backup/admin_backups" ]; then
    env | grep -v pass > $REPORT_FILE
    script_path=$(dirname $(realpath -s $0))
    script_name=$(basename -- "$0")
    MAIL_BIN="/usr/local/bin/mail"
    REPORT_DATE=`/usr/bin/date +%s`
    REPORT_PATH="$script_path/log"
    REPORT_FILE="$REPORT_PATH/$script_name-$REPORT_DATE.$RANDOM.log"
    MAIL_BIN="/usr/local/bin/mail"
    MYEMAIL="webmaster@sofibox.com"
    BACKUP_DATE_TIME_NOW="$(date '+%d-%m-%Y_%H-%M-%S')" #31-03-2020--11-56-16
    BACKUP_FOLDER_NAME_NOW="${username}_backup_$BACKUP_DATE_TIME_NOW" #admin123_backup-31-03-2020_11-56-16-AM
    backup_source="/home/$username/backups" #eg /home/admin/backups
    BACKUP_TAR_NAME="$backup_source/$BACKUP_FOLDER_NAME_NOW.tar.gz"
    BACKUP_TAR_NAME_ENCRYPTED="$backup_source/$BACKUP_FOLDER_NAME_NOW.tar.gz.enc"
    backup_destination="Backup/Server/earth.sofibox.com/user_backups/$BACKUP_FOLDER_NAME_NOW"
    WARNING_STATUS="OK"
    mv "$file" "$BACKUP_TAR_NAME"
    file="$BACKUP_TAR_NAME"
    mkdir -p $script_path/log
    find $REPORT_PATH -name "*.log" -mtime +2 -exec rm {} \;
    #check_valid_tar
    echo "Performing non-admin level backup ..." | tee -a $REPORT_FILE
    echo "Checking backup archive for corruption ... (this may take some time)" | tee -a $REPORT_FILE
    if gzip -t "$file" &>/dev/null; then
        echo "[$script_name | info]: OK, backup archive file of [$file] is valid" | tee -a $REPORT_FILE
    else
        echo "[$script_name | info]: Warning, backup archive file of [$file] is corrupted" | tee -a $REPORT_FILE
        rm -f "$f"
        echo "[$script_name | info]: Backup file [$file] has been removed" | tee -a $REPORT_FILE
        WARNING_STATUS="WARNING"
    fi
    #create_backup_dir
    echo "[$script_name | info]: Creating new backup directory in [onedrive] as [$backup_destination] ..." | tee -a $REPORT_FILE
    bash -o pipefail -c "rclone mkdir onedrive:$backup_destination | tee -a $REPORT_FILE"
    return_code=$?
    if [ $return_code  == 0 ]; then
        echo "[$script_name | info]: OK, new backup folder [$backup_destination] created at [onedrive]" | tee -a $REPORT_FILE
    else
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: [$return_code] Warning, something is wrong with the $return code while creating directory at [onedrive]" | tee -a $REPORT_FILE
    fi
    # encrypt backup file
    if [ -f $ENC_PASS ]; then # If encryption key exist we encrypt the backup tar.gz file to tar.gz.enc
        echo "[$script_name | info]: Encrypting backup file of [$file] as $BACKUP_TAR_NAME_ENCRYPTED ..." | tee -a $REPORT_FILE
        $ENCRYPT_SCRIPT $BACKUP_TAR_NAME $BACKUP_TAR_NAME_ENCRYPTED $ENC_PASS
        file=$BACKUP_TAR_NAME_ENCRYPTED
    else
        echo "[$script_name | info]: Warning, encryption key is not setup at [$ENC_PASS]. File upload operations aborted for security reason" | tee -a $REPORT_FILE
        echo "[$script_name | info]: Backup status: [$WARNING_STATUS]" | tee -a $REPORT_FILE
        $MAIL_BIN -s "[$script_name, $username | $WARNING_STATUS]: User-Level Backup Operation Report" $MYEMAIL < $REPORT_FILE
        exit 1
    fi

    #upload_backup_file (use move to send encrypted file and leave the unencrypted)
    bash -o pipefail -c "rclone move $file onedrive:$backup_destination/ --log-file=$REPORT_FILE --log-level INFO --stats-one-line -P --stats 2s"
    return_code=$?
    if [ $return_code  == 0 ]; then
        echo "[$script_name | info]: Success, backup file of [$file] has been successfully uploaded into [onedrive]" | tee -a $REPORT_FILE
    elif [ $return_code  == 1 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, syntax or usage error while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 2 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, error not otherwise categorised while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 3 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, directory not found while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 4 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, file not found while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 5 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, temporary error (one that more retires might fix) (Retry errors) while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 6 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, less serious errors (like 461 erros from dropbox) (NoRetry errors) while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 7 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, fatal error (one that more retries won't fix, like account suspended) (Fatal errors) while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 8 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, transfer exceeded - limit set by --max-transfer reached while performing file upload [$file]" | tee -a $REPORT_FILE
    elif [ $return_code  == 9 ]; then
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, operation successful, but no files transferred while performing file upload [$file]" | tee -a $REPORT_FILE
    else
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: Error, unknown error while performing file upload [$file]" | tee -a $REPORT_FILE
    fi
    echo "[$script_name | info]: Backup status: [$WARNING_STATUS]" | tee -a $REPORT_FILE
    #echo "[$script_name | info]: Email notification is set to [$MYEMAIL]" | tee -a $REPORT_FILE
    echo "===================================================================================="
    $MAIL_BIN -s "[$script_name, $username | $WARNING_STATUS]: User-Level Backup Operation Report" $MYEMAIL < $REPORT_FILE

fi