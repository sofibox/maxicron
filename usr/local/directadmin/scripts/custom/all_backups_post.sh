#!/bin/bash

# Directadmin all_backup_post.sh
# Upload backup created by user, admin and reseller to onedrive
# Author - Arafat Ali
global_start=$(date +%s.%N)
script_path=$(dirname $(realpath -s $0))
script_name=$(basename -- "$0")
MAIL_BIN="/usr/local/bin/mail"
REPORT_DATE=`/usr/bin/date +%s`
REPORT_PATH="$script_path/log"
REPORT_FILE="$REPORT_PATH/$script_name-$REPORT_DATE.$RANDOM.log"
MAIL_BIN="/usr/local/bin/mail"
MYEMAIL="webmaster@sofibox.com"
BACKUP_DATE_TIME_NOW="$(date '+%d-%m-%Y_%H-%M-%S')" #31-03-2020--11-56-16
BACKUP_FOLDER_NAME_NOW="admin_backups_$BACKUP_DATE_TIME_NOW" #system_backups-31-03-2020_11-56-16-AM
backup_destination="Backup/Server/earth.sofibox.com/admin_backups/$BACKUP_FOLDER_NAME_NOW"
WARNING_STATUS="OK"
MARK_UPLOAD="backup_files_uploaded_to_onedrive.txt"
ENC_PASS="/root/.enc_password"
ENCRYPT_SCRIPT="/usr/local/maxiscript/utils/encrypt_file.sh" # DA ENCRYPTION DEPRECATED use this instead
return_code=0
mkdir -p $script_path/log
#env | grep -v pass > $REPORT_FILE
cd $local_path # Directadmin defined backup location eg: local_path=/backup/admin_backups/Tuesday
currentPWD=$PWD # Save the current directory

find $REPORT_PATH -name "*.log" -mtime +2 -exec rm {} \;

function check_valid_tar {

    find_file=(`find $local_path -maxdepth 1 -name "*.tar.gz"`)
    if [ ${#find_file[@]} -gt 0 ]; then
        if [ ${#find_file[@]} == 1 ]; then
            echo  "[$script_name | info]: There is ${#find_file[@]} backup file in [$local_path]" | tee -a $REPORT_FILE
        else
            echo  "[$script_name | info]: There are ${#find_file[@]} backup files in [$local_path]" | tee -a $REPORT_FILE
        fi
        for f in *.tar.gz
        do
            if [ "$f" != "*.tar.gz" ]; then
                echo "[$script_name | info]: Checking backup file of [$f] for corruption ... (this may take some time):" | tee -a $REPORT_FILE
                if gzip -t "$f" &>/dev/null; then
                    echo "[$script_name | info]: OK, backup archive file of [$f] is valid" | tee -a $REPORT_FILE
                    echo "-----" | tee -a $REPORT_FILE
                    # encrypt backup file
                    if [ -f $ENC_PASS ]; then # If encryption key exist we encrypt the backup tar.gz file to tar.gz.enc using DA openssl encryption
                        echo "[$script_name | info]: Encrypting backup file of [$f] as ${f}.enc ..." | tee -a $REPORT_FILE
                        $ENCRYPT_SCRIPT "$f" "${f}.enc" "$ENC_PASS"
                        return_code=$?
                        if [ "$return_code" == 0 ]; then
                            echo  "[$script_name | info]: Successfully encrypted backup file of [$f] as ${f}.enc" | tee -a $REPORT_FILE
                            rm -f $f #Remove the unencrypted backup
                            echo  "[$script_name | info]: Unencrypted backup file of [$f] was deleted" | tee -a $REPORT_FILE
                        else
                            WARNING_STATUS="WARNING"
                            echo  "[$script_name | info]: Warning, unable to encrypt backup file of [$f]. OpenSSL error code is: $return_code" | tee -a $REPORT_FILE
                        fi
                    else
                        WARNING_STATUS="WARNING"
                        echo "[$script_name | info]: Warning, unable to read encryption key at [$ENC_PASS]. Please setup encryption password at [$ENC_PASS]" | tee -a $REPORT_FILE
                        echo "[$script_name | info]: Warning, backup files are not encrypted because unable to read encryption key at [$ENC_PASS]" | tee -a $REPORT_FILE
                        echo "[$script_name | info]: Warning, unencrypted backup files will stay in $local_path but it will not be uploaded into [onedrive] due to security reason" | tee -a $REPORT_FILE
                        #exit 1
                    fi
                else
                    WARNING_STATUS="WARNING"
                    echo "[$script_name | info]: Warning, backup archive file of [$f] is corrupted" | tee -a $REPORT_FILE
                    rm -f "$f"
                    echo "[$script_name | info]: Backup file [$f] has been removed" | tee -a $REPORT_FILE
                fi
            fi
        done
    else
        echo  "[$script_name | info]: OK, there is no backup files in [$local_path]" | tee -a $REPORT_FILE
    fi
    echo "[$script_name | info]: Finished checking valid tar backup files" | tee -a $REPORT_FILE
}


function create_backup_dir {
    echo "[$script_name | info]: Creating new backup directory in [onedrive] as [$backup_destination] ..." | tee -a $REPORT_FILE
    bash -o pipefail -c "rclone mkdir onedrive:$backup_destination | tee -a $REPORT_FILE"
    return_code=$?
    if [ $return_code  == 0 ]; then
        echo "[$script_name | info]: OK, new backup folder [$backup_destination] created at [onedrive]" | tee -a $REPORT_FILE
    else
        WARNING_STATUS="WARNING"
        echo "[$script_name | info]: [$return_code] Warning, something is wrong with the $return code while creating directory at [onedrive]" | tee -a $REPORT_FILE
    fi
}

function upload_all_backups {
    for f in *.tar.gz.enc #only upload encrypted enc file
    do
        if [ "$f" != "*.tar.gz.enc" ]; then
            echo "[$script_name | info]: Uploading the backup file [$f] into [onedrive] ... (this may take some time):" | tee -a $REPORT_FILE
            bash -o pipefail -c "rclone move $f onedrive:$backup_destination/ --log-file=$REPORT_FILE --log-level INFO --stats-one-line -P --stats 2s"
            return_code=$?
            if [ $return_code  == 0 ]; then
                echo "This backup folder is empty because it has been uploaded into onedrive" > $local_path/$MARK_UPLOAD
                echo "[$script_name | info]: Success, backup file of [$f] has been successfully uploaded into [onedrive]" | tee -a $REPORT_FILE
            elif [ $return_code  == 1 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, syntax or usage error while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 2 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, error not otherwise categorised while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 3 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, directory not found while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 4 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, file not found while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 5 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, temporary error (one that more retires might fix) (Retry errors) while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 6 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, less serious errors (like 461 erros from dropbox) (NoRetry errors) while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 7 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, fatal error (one that more retries won't fix, like account suspended) (Fatal errors) while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 8 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, transfer exceeded - limit set by --max-transfer reached while performing file upload [$f]" | tee -a $REPORT_FILE
            elif [ $return_code  == 9 ]; then
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, operation successful, but no files transferred while performing file upload [$f]" | tee -a $REPORT_FILE
            else
                WARNING_STATUS="WARNING"
                echo "[$script_name | info]: Error, unknown error while performing file upload [$f]" | tee -a $REPORT_FILE
            fi
        else
            echo "[$script_name | info]: No backup files found in [$local_path]" | tee -a $REPORT_FILE
        fi
    done
}

check_valid_tar
create_backup_dir
upload_all_backups
echo "[$script_name | info]: Backup status: [$WARNING_STATUS]" | tee -a $REPORT_FILE
echo "[$script_name | info]: Report file location: [$REPORT_FILE]" | tee -a $REPORT_FILE
echo "===================================================================================="
global_end=$(date +%s.%N)
dt=$(echo "$global_end - $global_start" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)

LC_NUMERIC=C printf "[$script_name | info]: Total runtime of [$script_name]: [%d:%02d:%02d:%02.4f]\n" $dd $dh $dm $ds | tee -a $REPORT_FILE

$MAIL_BIN -s "[$script_name | $WARNING_STATUS]: Admin-Level Backup Operation Report" $MYEMAIL < $REPORT_FILE

cd $currentPWD