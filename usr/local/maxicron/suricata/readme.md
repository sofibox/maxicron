This script will monitor suricata log file and notify you via email if somebody is doing suspicious activity (in realtime). The notification rules are triggered from suricata official and some 3rd parties rules. These are the (best) rules that I use with suricata:

```
[root@earth conf]# /home/****/maxipy/bin/suricata-update list-enabled-sources
14/10/2020 -- 13:06:50 - <Info> -- Using data-directory /var/lib/suricata.
14/10/2020 -- 13:06:50 - <Info> -- Using Suricata configuration /etc/suricata/suricata.yaml
14/10/2020 -- 13:06:50 - <Info> -- Using /usr/share/suricata/rules for Suricata provided rules.
14/10/2020 -- 13:06:50 - <Info> -- Found Suricata version 6.0.0 at /usr/bin/suricata.
Enabled sources:
  - tgreen/hunting
  - sslbl/ssl-fp-blacklist
  - ptresearch/attackdetection
  - etnetera/aggressive
  - oisf/trafficid
  - sslbl/ja3-fingerprints
  - https://gitlab.com/cybergon/timon-rules/raw/master/cybergon.rules
  - et/open
[root@earth conf]#

```
When a suspicious activity is triggered from an ip, you can block it with `maxicata -d $ip $optional_comment`. It will block in csf and this also will remove the ip's info from the main suricata log, so you won't be notified from the log file for blocked ip.

You can also view other system logs automatically based on specific ip. You can modify what log files should be included at `conf/alertfile.conf`

Note that, if you want to auto block the rule (which is not recommended because some rules can be false-positive), but you can make auto block. You need to configure suricata in `IPS mode` for this purpose. Suricata can work with iptable and CSF when it is in `IPS mode`. If you think you have a good blocking rules, then you can enable `IPS mode` in suricata, else just use suricata as `IDS mode`. 
In `IDS mode` you can learn about rules behaviour and make modification to some false positive rules or you can even create your own rule. So I recommend to use `IDS mode` in suricata for beginner.

Sample output:

[ Document is deprecated at this point because new code changes has been made.. will update soon ]
1) Start the process

```
[root@earth suricata]# maxicata -e
[maxicata]: Maxicata status:
------------
[maxicata]: Suricata running status: failed
[maxicata]: Maxicata-INOTIFYWAIT running status: inactive
[maxicata]: Maxicata-INOTIFYWAIT cached PID: N/A
------------
[maxicata]: Sending a start signal to start Maxicata-INOTIFYWAIT process ...
[maxicata]: The current Maxicata-INOTIFYWAIT PID count is: 0
[maxicata]: The cached PID for Maxicata-INOTIFYWAIT is: 0
[maxicata]: No inotifywait process is running. Restarting Maxicata-INOTIFYWAIT ...
[maxicata]: Sending a stop flag and kill signal to terminate Maxicata-INOTIFYWAIT...
[maxicata]: Sending a start flag to restart Maxicata-INOTIFYWAIT ...
[maxicata]: Starting suricata IDS/IPS main process ...
[maxicata]: Starting a new Maxicata-INOTIFYWAIT process to monitor system security ...
[maxicata]: Finished executing Maxicata to start a new inotifywait process. Run [ maxicata -s ] to see running status

--- if process is already running it will let another process running ---

[root@earth ~]# maxicata -e
[maxicata]: Maxicata status:
------------
[maxicata]: Suricata running status: active
[maxicata]: Maxicata running status: active
[maxicata]: Maxicata-INOTIFYWAIT cached PID: 4079
------------
[maxicata]: Sending a start signal to start Maxicata-INOTIFYWAIT process ...
[maxicata]: The current Maxicata-INOTIFYWAIT PID count is: 1
[maxicata]: The cached PID for Maxicata-INOTIFYWAIT is: 4079
---------------------------
--- (1) PID: 4079 ---
Process id details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
   4079   6524 root     root     inotifywait     inotifywait -q /usr/local/maxicron/suricata/conf/p_flag /var/log/suricata/maxicata.log -e modify -m
[maxicata]: OK, current inotifywait PID [4079] is matched with the cached inotifywait PID [4079]
[maxicata]: Maxicata-INOTIFYWAIT is already running ... not starting a new one!
[root@earth ~]#

```

2) Check if process is running

```
[root@earth suricata]# maxicata -s
[maxicata]: Maxicata status:
------------
[maxicata]: Suricata running status: active
[maxicata]: Maxicata running status: active
[maxicata]: Maxicata-INOTIFYWAIT cached PID: 1128299
------------

```

3) Stop the process

```
[root@earth suricata]# maxicata -x
[maxicata]: Maxicata status:
------------
[maxicata]: Suricata running status: active
[maxicata]: Maxicata running status: active
[maxicata]: Maxicata-INOTIFYWAIT cached PID: 1128299
------------
[maxicata]: Sending a stop signal to terminate Maxicata-INOTIFYWAIT process (this may take a few moment)...
[maxicata]: Check email youremail@server.com to see the termination report or run [ maxicata -s ] to see running status
[root@earth log]#
```

Block an IP:

maxicata -d <ip> <extra_optional_comment>
```
[root@earth suricata]# maxicata -d 103.131.71.205 Has abuse IP of 100%
[maxicata]: Blocking 103.131.71.205 IP address [103.131.71.205/Viet Nam] in CSF ...
[maxicata]: CSF comment block is set as follow:
------------
maxicata [Manual Block from Maxicata][IP: 103.131.71.205/Viet Nam] Has abuse IP of 100%
------------
Adding 103.131.71.205 to csf.deny and iptables DROP...
csf: IPSET adding [103.131.71.205] to set [chain_DENY]
[maxicata]: Now, restarting CSF ...
[maxicata]: Sending a sleep flag to sleep Maxicata-INOTIFYWAIT for 7 second(s) ...

Report line to remove from /var/log/suricata/maxicata.log:
---------
Removed OK: 12] alert:yes 10/21/2020-16:06:21.822472 103.131.71.205 sofibox_local_ip 4.0 SSLBL: Malicious

---------
1 record has been removed from /var/log/suricata/maxicata.log

```

Find information about an IP:

```
maxicata -i 123.123.123.123

```

#Suspicious Report from email

1) Example: IP with low score:

```

[Suricata | ALERT: Medium Risk (1) ]: New Suspicious IP from [104.244.42.2/United States] @ earth.sofibox.com [Email header]

Warning, new suspicious IP from [104.244.42.2/United States]
---------------------
AbuseIPDB Security Info [New IP Info]
---------------------
Abuse Confidence Score: 6%
IP: 104.244.42.2
ISP: Twitter Inc.
Usage Type: Commercial
Domain Name: twitter.com
Country Name/Code: United States/US
AIPDB Report Information:
*---*
1) This IP address has been reported a total of 4 time(s) from 1 distinct sources. 104.244.42.2 most recent report was [2020-11-04T13:39:53+00:00]. More info at: https://www.abuseipdb.com/check/104.244.42.2
2) IP is not set to report to AIPDB because it doesn't have enough bad score
3) Notice, REPORTED IP cache is empty at this moment
*---*

---------------------
Suricata Security Info:
---------------------
Risk Priority Level: Medium Risk (3/5)
Source/Inbound(1) + Destination/Outbound(0) = 1 record(s)
Blacklist SPAM Count: [new] blacklisted (0)
Suspicious Log Count: 0
AIPDB Attack Category Code: 15 [More info at: http://abuseipdb.com/categories]
Recommend Action: Unable to determine more reputation score for this IP. Please audit this IP manually

---------------------
Your Action:
---------------------
-- Coming soon --
---------------------
Suspicious IP web references:
---------------------
1) https://www.abuseipdb.com/check/104.244.42.2
2) https://infobyip.com/ip-104.244.42.2.html
3) https://stopforumspam.com/ipcheck/104.244.42.2
4) https://www.spamhaus.org/query/ip/104.244.42.2
5) https://talosintelligence.com/reputation_center/lookup?search=104.244.42.2
6) http://www.borderware.com/lookup.php?ip=104.244.42.2
7) http://104.244.42.2

---------------------
Current suspicious activity from [104.244.42.2/United States]:
---------------------
|N 0 |ALERT: y |PRIO: 3 |TIME: 11/09/2020-02:01:27.111056 |IPLAYER: {6} 104.244.42.2 {: 443 } -> LOCAL_IP {: 46680 } |GSR: [:G 1 :S 2230003 :R 1 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA TLS invalid handshake message

---
---------------------

-----------------
History of suspicious activities from [104.244.42.2/United States] with [alert=yes]:
-----------------
|N 0 |ALERT: y |PRIO: 3 |TIME: 11/09/2020-02:01:27.111056 |IPLAYER: {6} 104.244.42.2 {: 443 } -> LOCAL_IP {: 46680 } |GSR: [:G 1 :S 2230003 :R 1 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA TLS invalid handshake message
---

-----------------
History of suspicious activities from [104.244.42.2/United States] with [alert=no]:
---------------------
---
---------------------

---------------------
Other suspicious log files from this IP: [104.244.42.2/United States]:
---------------------
1) EXIM MAIL MAIN LOG
[maxicata]: Found this IP at: /var/log/exim/mainlog:
---------
[maxicata]: Latest 10 records:
---
2020-11-08 19:56:32 1kbjJA-008nO9-Ru <= root@earth.sofibox.com U=root P=local S=17512 T="[Suricata | ALERT: Medium Risk (43) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 19:56:34 1kbjJC-008nXb-0c <= root@earth.sofibox.com U=root P=local S=17741 T="[Suricata | ALERT: Medium Risk (44) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:34:53 1kblmP-00AML5-Cl <= root@earth.sofibox.com U=root P=local S=17981 T="[Suricata | ALERT: Medium Risk (45) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:34:54 1kblmQ-00AMUS-Iu <= root@earth.sofibox.com U=root P=local S=18195 T="[Suricata | ALERT: Medium Risk (46) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:34:55 1kblmR-00AMdo-MY <= root@earth.sofibox.com U=root P=local S=18433 T="[Suricata | ALERT: Medium Risk (47) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:34:56 1kblmS-00AMnF-TA <= root@earth.sofibox.com U=root P=local S=18662 T="[Suricata | ALERT: Medium Risk (48) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:52:49 1kbm3l-00ANzS-Ty <= root@earth.sofibox.com U=root P=local S=18900 T="[Suricata | ALERT: Medium Risk (49) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:52:51 1kbm3n-00AO8q-6M <= root@earth.sofibox.com U=root P=local S=19129 T="[Suricata | ALERT: Medium Risk (50) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:52:52 1kbm3o-00AOIC-E6 <= root@earth.sofibox.com U=root P=local S=19367 T="[Suricata | ALERT: Medium Risk (51) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-08 22:52:53 1kbm3p-00AORZ-Jv <= root@earth.sofibox.com U=root P=local S=19596 T="[Suricata | ALERT: Medium Risk (52) ]: Existing Suspicious\n IP from [104.244.42.2/United States] @ e" from <root@earth.sofibox.com> for webmaster@sofibox.com
---------

2) SURICATA FAST LOG
[maxicata]: Found this IP at: /var/log/suricata/fast.log:
---------
[maxicata]: Latest 10 records:
---
11/09/2020-02:01:27.111056  [**] [1:2230003:1] SURICATA TLS invalid handshake message [**] [Classification: Generic Protocol Command Decode] [Priority: 3] {TCP} 104.244.42.2:443 -> 172.104.50.185:46680
11/09/2020-02:01:27.111056  [**] [1:2230010:1] SURICATA TLS invalid record/traffic [**] [Classification: Generic Protocol Command Decode] [Priority: 3] {TCP} 104.244.42.2:443 -> 172.104.50.185:46680
---------

SUSP_L_COUNT: 0
---
---------------------

```

2) Example: IP with high score:

```
[Suricata | ALERT: Medium Risk (1) ]: New Suspicious IP from [212.70.149.84/United Kingdom of Great Britain and Northern Ireland] @ earth.sofibox.com [Email header]

Warning, new suspicious IP from [212.70.149.84/United Kingdom of Great Britain and Northern Ireland]
---------------------
AbuseIPDB Security Info [Cached IP Info]
---------------------
Abuse Confidence Score: 100%
IP: 212.70.149.84
ISP: Global Communication Net Plc
Usage Type: Fixed Line ISP
Domain Name: gcn.bg
Country Name/Code: United Kingdom of Great Britain and Northern Ireland/GB
AIPDB Report Information:
*---*
1) This IP address has been reported a total of 7763 time(s) from 128 distinct sources. 212.70.149.84 most recent report was [2020-11-11T07:09:00+00:00]. More info at: https://www.abuseipdb.com/check/212.70.149.84
2) OK, 212.70.149.84 has been reported successfully with no error

3) Notice, REPORTED IP cache is empty at this moment
*---*

---------------------
Suricata Security Info:
---------------------
Risk Priority Level: Medium Risk (3/5)
Source/Inbound(0) + Destination/Outbound(1) = 1 record(s)
Blacklist SPAM Count: [cached] blacklisted (14)
Suspicious Log Count: 2
AIPDB Attack Category Code: 11 [More info at: http://abuseipdb.com/categories]
Info & Recommend Action: deny failed: 212.70.149.84 is in already in the deny file /etc/csf/csf.deny 1 times. If this IP is still giving you this alert, See `Your Action` below on how to suppress the alert:

---------------------
Your Action:
---------------------
Command to block this IP:
maxicata -c deny 212.70.149.84 'Suricata_CSF_Manual_Block | GeoIP: Global Communication Net Plc/Fixed Line ISP/gcn.bg/GB| Abuse_Percentage:100% | Alert_Level: Medium Risk |RBL_Blacklisted_Count: 14| Inbound_Outbound: 1'

Few commands to suppress this IP:
1) Suppress this IP from reporting in suricata:
maxicata -sip 212.70.149.84 2260002

2) Suppress this type of suspicious message:
Coming soon
---------------------
Suspicious IP web references:
---------------------
1) https://www.abuseipdb.com/check/212.70.149.84
2) https://infobyip.com/ip-212.70.149.84.html
3) https://stopforumspam.com/ipcheck/212.70.149.84
4) https://www.spamhaus.org/query/ip/212.70.149.84
5) https://talosintelligence.com/reputation_center/lookup?search=212.70.149.84
6) http://www.borderware.com/lookup.php?ip=212.70.149.84
7) http://212.70.149.84

---------------------
Current suspicious activity from [212.70.149.84/United Kingdom of Great Britain and Northern Ireland]:
---------------------
|N 0 |ALERT: y |PRIO: 3 |TIME: 11/12/2020-00:24:40.740242 |IPLAYER: {6} LOCAL_IP {: 25 } -> 212.70.149.84 {: 11660 } |GSR: [:G 1 :S 2260002 :R 1 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA Applayer Detect protocol only one direction

---
---------------------

-----------------
History of suspicious activities from [212.70.149.84/United Kingdom of Great Britain and Northern Ireland] with [alert=yes]:
-----------------
|n 0 |time: 11/12/2020-00:24:40.740242 |critical_rpt: y |prio: 3 |score: bad |rbl_count: 14 |susp_log_count: 2 |is_csf_blocked: no |aipdb_score: 100 |ip_layer: {6} LOCAL_IP {: 25 } -> 212.70.149.84 {: 11660 } |GSR: [:G 1 :S 2260002 :R 1 ] |class: Generic Protocol Command Decode |msg: SURICATA Applayer Detect protocol only one direction
---

-----------------
History of suspicious activities from [212.70.149.84/United Kingdom of Great Britain and Northern Ireland] with [alert=no]:
---------------------
---
---------------------

---------------------
Other suspicious log files from this IP: [212.70.149.84/United Kingdom of Great Britain and Northern Ireland]:
---------------------
1) CSF TEMPORARY IP BAN LOCATION
[maxicata]: Found this IP at: /var/lib/csf/csf.tempip:
---------
[maxicata]: Latest 10 records:
---
212.70.149.84|1|1605039830|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.84|1|1605078309|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.84|1|1605078449|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.84|1|1605079336|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.84|1|1605088331|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.84|1|1605088446|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.84|1|1605111567|(smtpauth) Failed SMTP AUTH login from 212.70.149.84 (GB/United Kingdom/-): 1 in the last 3600 secs
---------

2) EXIM MAIL MAIN LOG
[maxicata]: Found this IP at: /var/log/exim/mainlog:
---------
[maxicata]: Latest 10 records:
---
2020-11-11 18:08:58 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=mailtest@sofibox.com)
2020-11-11 18:10:03 1kcn4l-00CZqq-KI <= root@earth.sofibox.com U=root P=local S=14502 T="[Suricata | ALERT: Medium Risk (11) ]: Existing Suspicious\n IP from [212.70.149.84/United Kingdom of" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-11 18:10:11 1kcn4t-00CaLp-6D <= root@earth.sofibox.com U=root P=local S=14761 T="[Suricata | ALERT: Medium Risk (12) ]: Existing Suspicious\n IP from [212.70.149.84/United Kingdom of" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-11 18:10:17 1kcn4z-00Caqb-2J <= root@earth.sofibox.com U=root P=local S=15150 T="[Suricata | ALERT: Medium Risk (13) ]: Existing Suspicious\n IP from [212.70.149.84/United Kingdom of" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-11 18:10:20 1kcn52-00Cb5n-0G <= root@earth.sofibox.com U=root P=local S=15422 T="[Suricata | ALERT: Medium Risk (14) ]: Existing Suspicious\n IP from [212.70.149.84/United Kingdom of" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-11 18:10:26 1kcn58-00CbaO-Im <= root@earth.sofibox.com U=root P=local S=15786 T="[Suricata | ALERT: Medium Risk (15) ]: Existing Suspicious\n IP from [212.70.149.84/United Kingdom of" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-11 18:10:32 1kcn5E-00Cc5E-Ps <= root@earth.sofibox.com U=root P=local S=16158 T="[Suricata | ALERT: Medium Risk (16) ]: Existing Suspicious\n IP from [212.70.149.84/United Kingdom of" from <root@earth.sofibox.com> for webmaster@sofibox.com
2020-11-11 23:31:44 SMTP command timeout on connection from [212.70.149.84]
2020-11-12 00:19:23 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=sunshine@sofibox.com)
2020-11-12 00:19:27 1kcsqF-005Fkh-UC <= root@earth.sofibox.com U=root P=local S=1347 T="[lfd] Failures 1 (smtpauth) on earth.sofibox.com: blocked 212.70.149.84 (GB/United Kingdom/-)" from <root@earth.sofibox.com> for root
---------

3) EXIM MAIL REJECT LOG
[maxicata]: Found this IP at: /var/log/exim/rejectlog:
---------
[maxicata]: Latest 10 records:
---
2020-11-11 18:06:18 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=mw@sofibox.com)
2020-11-11 18:06:41 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=msa@sofibox.com)
2020-11-11 18:07:03 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=moon@sofibox.com)
2020-11-11 18:07:27 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=mn@sofibox.com)
2020-11-11 18:07:48 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=migration@sofibox.com)
2020-11-11 18:08:16 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=microsites@sofibox.com)
2020-11-11 18:08:34 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=mds@sofibox.com)
2020-11-11 18:08:58 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=otrs@sofibox.com)
2020-11-11 18:08:58 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=mailtest@sofibox.com)
2020-11-12 00:19:23 login authenticator failed for (User) [212.70.149.84]: 535 Incorrect authentication data (set_id=sunshine@sofibox.com)
---------

4) SURICATA FAST LOG
[maxicata]: Found this IP at: /var/log/suricata/fast.log:
---------
[maxicata]: Latest 10 records:
---
11/12/2020-00:24:40.740242  [**] [1:2260002:1] SURICATA Applayer Detect protocol only one direction [**] [Classification: Generic Protocol Command Decode] [Priority: 3] {TCP} 172.104.50.185:25 -> 212.70.149.84:11660
---------

SUSP_L_COUNT: 2
---
---------------------


```