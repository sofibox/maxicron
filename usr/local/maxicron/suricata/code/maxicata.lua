-- This is a lua script that is used by suricata to generate custom report and send email
-- Required: RBL scanner, Suricata v5+ - v6+, Maxicata | Works in CentOS 8
-- Author: Arafat Ali | Email: arafat@sofibox.com

--[[
   TODO 1 - Write autoblock syntax suggestion to copy paste into terminal if IP is not autoblock (done at non-critical report, not done at critical report)
   TODO 2 - Small bug but can be fix where auto block for IPv6 has problem in which CSF automatically convert long IP to short form when do blocking (done need testing)
          - Refer to flag mail at draft. Example IP: 2607:5300:0061:0eda:0000:0000:0000:0000 (done need testing)
          - Blocking is working but the ip became short form in /etc/csf/csf.deny, so grep has problem determine that ipv6 entry in csf.. (just the other suspicious log not appear) (done need testing)
   TODO 3 - Make Maxicata blocking command (maxicata -d <ip>) to be able to block a network range in CSF (need to calculate CIDR). For example, this IP: 1.2.3.1 and 1.2.3.2 and 1.2.3.3 can be blocked by using this form 1.2.3.0/24 (blocking ip -d done)

   TODO 4 - Create whitelist database to store whitelist IP (that will never trigger alert) unless we clear the whitelist IP.
          - Note that I can use threshold.config to suppress the suspicious alert and this will also prevent CSF from autoblock.
          - By using threshold.config approach, we only need 3 things: the signature ID, the generation ID and the IP address.. We need add comment on top of the file for new attack ID
          - But.. I think the best is not to use threshold.config approach but create my own whitelist file in db folder because we still need this report in fast.log not to globally suppress it using threshold.config.

   T̶O̶D̶O̶ ̶5̶ ̶-̶ ̶F̶i̶x̶ ̶t̶h̶e̶ ̶d̶a̶t̶a̶ ̶t̶y̶p̶e̶ ̶n̶u̶l̶l̶,̶ ̶w̶h̶e̶r̶e̶ ̶a̶i̶p̶d̶b̶ ̶r̶e̶t̶u̶r̶n̶ ̶a̶ ̶t̶e̶x̶t̶ ̶'̶n̶u̶l̶l̶'̶ ̶n̶o̶t̶ ̶t̶h̶e̶ ̶d̶a̶t̶a̶t̶y̶p̶e̶ ̶o̶f̶ ̶n̶u̶l̶l̶ ̶(̶d̶o̶n̶e̶ ̶b̶u̶t̶ ̶n̶e̶e̶d̶ ̶t̶e̶s̶t̶i̶n̶g̶)̶
   T̶O̶D̶O̶ ̶6̶ ̶-̶ ̶F̶i̶x̶ ̶e̶r̶r̶o̶r̶ ̶s̶e̶a̶r̶c̶h̶ ̶w̶i̶t̶h̶ ̶G̶R̶E̶P̶ ̶w̶h̶e̶n̶ ̶t̶h̶e̶ ̶I̶P̶ ̶i̶s̶ ̶1̶.̶2̶.̶3̶.̶5̶ ̶a̶n̶d̶ ̶1̶.̶2̶.̶3̶.̶5̶4̶ ̶(̶d̶o̶n̶e̶ ̶b̶u̶t̶ ̶n̶e̶e̶d̶ ̶t̶e̶s̶t̶i̶n̶g̶)̶
   TODO 7 - Setup cache IP and blacklist cache scan (real cache) - pull data from cache first, if not exist then pull from API [done need testing]
   T̶O̶D̶O̶ ̶8̶ ̶-̶ ̶T̶r̶a̶n̶s̶f̶o̶r̶m̶ ̶a̶l̶l̶ ̶w̶r̶a̶p̶p̶e̶r̶ ̶f̶u̶n̶c̶t̶i̶o̶n̶ ̶i̶n̶t̶o̶ ̶m̶a̶x̶i̶c̶a̶t̶a̶ ̶-̶c̶ ̶<̶c̶o̶m̶m̶a̶n̶d̶>̶ ̶t̶o̶ ̶r̶e̶d̶u̶c̶e̶ ̶c̶o̶d̶e̶ ̶i̶n̶ ̶l̶u̶a̶ ̶r̶e̶p̶o̶r̶t̶ ̶(̶d̶o̶n̶e̶)̶
   TODO 9 - Clear report cache by timestamp for blacklist_scan and aipdb cache. For example clear after 1 or 2 hours (every cache already has timestamp) (done need testing)
   ̶ ̶T̶O̶D̶O̶ ̶1̶0̶ ̶-̶ ̶M̶a̶r̶k̶ ̶R̶B̶L̶ ̶b̶l̶a̶c̶k̶l̶i̶s̶t̶ ̶s̶c̶a̶n̶ ̶a̶s̶ ̶p̶e̶n̶d̶i̶n̶g̶ ̶s̶c̶a̶n̶ ̶f̶o̶r̶ ̶e̶a̶c̶h̶ ̶I̶P̶.̶ ̶T̶h̶i̶s̶ ̶I̶P̶ ̶w̶i̶l̶l̶ ̶n̶o̶t̶ ̶r̶e̶c̶e̶i̶v̶e̶ ̶a̶n̶o̶t̶h̶e̶r̶ ̶s̶a̶m̶e̶ ̶s̶c̶a̶n̶ ̶i̶f̶ ̶i̶t̶'̶s̶ ̶d̶o̶i̶n̶g̶ ̶a̶ ̶s̶c̶a̶n̶ ̶b̶u̶t̶ ̶i̶t̶ ̶a̶l̶l̶o̶w̶s̶ ̶a̶n̶o̶t̶h̶e̶r̶ ̶I̶P̶ ̶t̶o̶ ̶d̶o̶ ̶s̶c̶a̶n̶ ̶a̶t̶ ̶t̶h̶e̶ ̶s̶a̶m̶e̶ ̶t̶i̶m̶e̶ ̶(̶d̶o̶n̶e̶ ̶b̶u̶t̶ ̶n̶e̶e̶d̶ ̶t̶e̶s̶t̶i̶n̶g̶)̶.̶
]] --

-- This is a popen wrapper function used to handle and destory file handler for readline output command
-- Without this function code will be messy with handle:close() method when using popen function
function io.popen_readline(cmd)
    local handle = io.popen(cmd)
    local result = handle:read("*l")
    handle:close()
    return result
end

-- This is a popen wrapper function used to handle and destory file handler for readall output command
-- Without this function code will be messy with handle:close() method when using popen function
function io.popen_readall(cmd)
    local handle = io.popen(cmd)
    local result = handle:read("*a")
    handle:close()
    return result
end

-- This is a popen wrapper function used to handle and destory file handler (no output to return)
function io.popen_x(cmd)
    local handle = io.popen(cmd)
    handle:close()
end

-- This is an OS specific function useful to sleep suricata statement in this script. Similar to sleep(n) in bash
function sleep(n)
    os.execute("sleep " .. tonumber(n))
end

-- This function is used whether to display log function in suricata.log or not (For Debug)
-- To enable set enable_log = true
function logNotice(logstr)
    if enable_log == true then
        log_pattern = "Maxicata: " .. logstr
        SCLogNotice(log_pattern)
        if enable_self_log == true then
            maxicata_rpt:write(log_pattern .. "\n")
            maxicata_rpt:write("\n")
            maxicata_rpt:flush()
        end
    end
end

function csf_execute_block(rule)
    logNotice("=== START CSF Block Status ===")
    if enable_csf_auto_block == true then
        logNotice("CSF auto block is enabled")
        -- Blocking this IP using maxicata
        cmd_auto_blockstr = "maxicata -c deny " .. susp_ip .. " 'Suricata_Blocked by " .. rule .. " | GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_code .. "| Abuse_Percentage:" .. aipdb_abuse_score
                .. "% | Alert_Level: " .. alert_level .. " |RBL_Blacklisted_Count: " .. blacklist_count .. "| Inbound_Outbound: " .. src_dst_count .. " # do not delete'"
        csf_block_status = io.popen_readline(cmd_auto_blockstr)
        -- This one just regard that the blocking status = true
        if string.match(csf_block_status, "CSF has already blocked this IP") then -- This string we obtained from maxicata -c deny if IP is already exist in /etc/csf/csf.deny
            logNotice("The already blocked this. Block Status is: " .. csf_block_status)
            is_csf_blocked = "yes"
        end
        alert_level_action = csf_block_status .. ". If this IP is still giving you this alert, See `Your Action` below on how to suppress the alert:"
    else
        logNotice("CSF auto block is disabled")
        alert_level_action = "CSF autoblock is not set but the bad rule has been triggered for this IP. Block this IP immediately" -- Give action when enable_csf_auto_block is not set
    end
    logNotice("=== END CSF Block Status ===")
end

-- This function is used to get information about an IP from abuseipdb.com using API. It has wrapper function from maxicata -c <command>
function set_aipdb_cached_data(ip)
    logNotice("=== START AIPDB cache IP info ===")
    logNotice("Checking existing IP " .. ip .. " for cache")
    -- First make sure that the IP we want to cache is not exist in the cache file. Use wrapper: maxicata -c is-aipdb-ip-cached <ip>
    check_ip_cached = tonumber(io.popen_readline("maxicata -c is-aipdb-ip-cached " .. ip))
    logNotice("Check IP cached return " .. check_ip_cached .. " [0=exist, 1=does not exist]")
    if check_ip_cached == 1 then -- Meaning if an IP does not exist in cache,
        logNotice("Notice, the IP " .. ip .. " is not cached in database file!")
        logNotice("Now caching IP " .. ip .. " info from AIPDB (this may take sometimes) ... ")
        io.popen_x("maxicata -c aipdb-cache-ipinfo " .. ip)
        -- Set it as a new IP label for the first time
        aipdb_cached = "new"
    else
        logNotice("OK, the IP " .. ip .. " is cached in database file!")
        -- Set it as an old IP label because the cached is inside the database
        aipdb_cached = "cached"
    end
    -- Then after the above condition, we read the IP cache information and assign each into variable.
    -- Usage: maxicata -c aipdb-get-cached-var <ip> <variable_field>
    -- Example: maxicata -c aipdb-get-cached-var 1.1.1.153 abuse_score, will return abuse_score value
    logNotice("Assigning all variables from cached file ...")
    aipdb_ip = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " ip")
    aipdb_iswhitelisted = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " iswhitelisted")
    aipdb_abuse_score = tonumber(io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " abuse_score"))
    aipdb_isp = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " isp")
    aipdb_usage_type = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " usage_type")
    aipdb_domain = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " domain")
    aipdb_country_name = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " country_name")
    aipdb_country_code = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " country_code")
    aipdb_total_report = tonumber(io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " total_report"))
    aipdb_distinct_report = tonumber(io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " distinct_report"))
    aipdb_last_report = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " last_report")
    aipdb_time = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " time")

    -- Debug variable in log (incase concatenation problem, for example string cannot concatenate with nil or number, then separate the variable from string)
    logNotice("This is a(n)" .. aipdb_cached .. " ip information:")
    logNotice("---------------------START------------------------")
    logNotice("aipdb_ip: " .. aipdb_ip .. "| type: " .. type(aipdb_ip))
    logNotice("aipdb_iswhitelisted: " .. aipdb_iswhitelisted .. "| type: " .. type(aipdb_iswhitelisted))
    logNotice("aipdb_abuse_score: " .. aipdb_abuse_score .. "| type: " .. type(aipdb_abuse_score))
    logNotice("aipdb_isp: " .. aipdb_isp .. "| type: " .. type(aipdb_isp))
    logNotice("aipdb_usage_type: " .. aipdb_usage_type .. "| type: " .. type(aipdb_usage_type))
    logNotice("aipdb_domain: " .. aipdb_domain .. "| type: " .. type(aipdb_domain))
    logNotice("aipdb_country_name: " .. aipdb_country_name .. "| type: " .. type(aipdb_country_name))
    logNotice("aipdb_country_code: " .. aipdb_country_code .. "| type: " .. type(aipdb_country_code))
    logNotice("aipdb_total_report: " .. aipdb_total_report .. "| type: " .. type(aipdb_total_report))
    logNotice("aipdb_distinct_report: " .. aipdb_distinct_report .. "| type: " .. type(aipdb_distinct_report))
    logNotice("aipdb_last_report: " .. aipdb_last_report .. "| type: " .. type(aipdb_last_report))
    logNotice("aipdb_time: " .. aipdb_time .. "| type: " .. type(aipdb_time))
    logNotice("----------------------END-------------------------")
    logNotice("==== END AIPDB cache IP info ====")
end

function get_attack_category(ip)
    logNotice("=== START suspicious attack category ===")
    -- maxicata -c generate_susp_log <ip> or maxicata -c gsl <ip>
    -- This function will look for an IP for suspected log found in system, if found, it will then write a report for that IP for each log path,
    -- It also will categorize each log for reporting and this function return log category number in sequence to be used with AIPDB report.
    -- If no category found, it will return 0
    report_susp_log = io.popen_readline("maxicata -c generate_susp_log " .. ip)
    logNotice("Suspicious log category is: " .. report_susp_log)
    logNotice("==== END log suspicious attack category ====")
    return report_susp_log
end

function get_susp_log_count(ip)
    logNotice("=== START suspicious log count status ===")
    -- maxicata -c get-susp-log-count <ip> or maxicata -c gslc <ip>
    -- This function will write a suspected log file for an IP into a log file and then it will return the number of log count
    check_susp_log_count = io.popen_readline("maxicata -c get-susp-log-count " .. ip)
    logNotice("Suspicious log count for IP " .. ip .. " is " .. check_susp_log_count .. "\n")
    logNotice("==== END suspicious log count status ====")
    return check_susp_log_count
end


-- This function will scan IP for blacklist status from various RBL website using Maxicata wrapper
-- There is also a cache file to speed up checking in this script. The reason is to reduce the time scan.
-- If cache not found in database, we mark the scan in a file to set pending scan status (because if we wait, it would take 10-30 seconds)
function blacklist_scan(ip)
    logNotice("=== START blacklist_scan status ====")
    if enable_rbl_scan == true then
        logNotice("RBL-Blacklist scan is enabled")
        logNotice("Checking if IP is in blacklisted file ...")
        -- This function maxicata -c is-ip-blacklisted returns the backlist count or nil (if no result)
        check_ip_blacklisted = io.popen_readline("maxicata -c is-ip-blacklisted " .. ip)
        if check_ip_blacklisted == nil then -- Meaning if the IP does not exist in blacklist database (not blacklisted),
            -- We run the blacklist scanner at the background
            -- cmdstr = "nohup bash maxicata -c scan-bl-q " .. ip .. "  </dev/null >/dev/null 2>&1 & -- (Temporarily not using background)
            logNotice("IP is not in blacklisted file. Scanning IP " .. ip .. " for blacklist (this may take sometime) ...")
            bl_count = io.popen_readline("maxicata -c scan-bl-q " .. ip)
            blacklist_status = "[new] blacklisted"
            blacklist_count = bl_count
            logNotice("New blacklist count is: " .. bl_count)
        else
            logNotice("IP is in blacklisted cached file")
            blacklist_status = "[cached] blacklisted"
            blacklist_count = check_ip_blacklisted
            logNotice("Cache blacklist count is: " .. check_ip_blacklisted)
        end
        -- Fix if the blacklist_count has null value (especially coming from the scan) - the scan can finish without blacklist and return nil.
        -- Update: Fixed, Maxicata -c scan-bl-q will not return null anymore from the wrapper but just leave this ifelse condition here for safety.
        if blacklist_count == nil then
            logNotice("Blacklist count is null and will now set as 0")
            blacklist_count = 0
        end
    else
        logNotice("RBL-Blacklist scan is disabled")
    end
    logNotice("==== END blacklist_scan status =====")
end

-- This is an init function that is requires by Suricata to define what data need to display
function init()
    local needs = {}
    needs["type"] = "packet"
    needs["filter"] = "alerts"
    return needs
end

function setup()
    -- Set enable RBL scan
    enable_rbl_scan = true
    -- Set to suppress msg
    enable_supp_msg = true --or false
    -- enable_log is use to disable or enable log in suricata.log for debugging purpose:
    enable_log = true -- or false (Disable for good performance)
    -- enable_self_log is used to write log based only from lua script (ignore the suricata system log)
    enable_self_log = true -- or false. You need enable_log = true to use this (Disable for good performance)
    -- enable_csf_auto_block is use to set whether to auto block IP using CSF (through wrapper: maxicata -d <ip>)
    enable_csf_auto_block = true
    -- send report to this email
    email = "arafat@sofibox.com"
    -- set the hostname
    hostname = "mars.codegix.com"
    localipv4 = "your-local-ipv4-server-same-as-what-reported-in-suricata"
    localipv6 = "your-localipv6-server-same-as-what-reported-in-suricata"
    clear_ip_cache_duration = "900" -- in second (AIPDB limit 15 minutes per-report. 15 minutes = 900 seconds)
    aipdb_cached = "new" --or cached
    api_cached_desc = "API cached description is not available"
    alert = "n" -- or y
    alert_level = "Unkown Alert Level"
    alert_level_action = "Alert level action is not available"
    alert_cache_subject = "Aler cache subject is not available"

    -- Category to report in AbuseIPDB
    -- aipdb_enable_report is use to enable auto report that has bad score into AIPDB web database
    aipdb_enable_report = true
    surCategory = "15" -- This is a default category in AIPDB (Hacking). Visit abuseipdb.com/categories for more category
    aipdb_report_desc = "AIPDB Report Description is not available"
    score_status = "N/A" -- ok or bad or unknown (Initial N/A)
    -- Initialize IP suppress list variable
    suppress_ip = "N/A"
    -- suppress_sid = "N/A"
    -- suppress_time = "N/A"
    -- Initialize cached variables from AIPDB before function call
    susp_ip = "N/A"
    aipdb_ip = "N/A"
    aipdb_iswhitelisted = "N/A"
    aipdb_abuse_score = tonumber("0")
    aipdb_isp = "N/A"
    aipdb_usage_type = "N/A"
    aipdb_domain = "N/A"
    aipdb_country_name = "N/A"
    aipdb_country_code = "N/A"
    aipdb_total_report = tonumber("0")
    aipdb_distinct_report = tonumber("0")
    aipdb_last_report = "N/A"
    aipdb_time = "N/A"

    -- Blacklist and suspicious log count
    blacklist_count = 0
    blacklist_status = "pending scan" -- or blacklisted

    -- The main Maxicata lua log (For debugging)
    maxicata_log = SCLogPath() .. "maxicata.log"
    maxicata_rpt = assert(io.open(maxicata_log, "a"))

    -- The main Maxicata lua log (accept both critical and non critical alert) (ok)
    maxicata_yn_log = SCLogPath() .. "maxicata-yn.log"
    maxicata_yn_rpt = assert(io.open(maxicata_yn_log, "a"))

    -- The main Maxicata alert for yes only (ok)
    maxicata_y_log = SCLogPath() .. "maxicata-y.log"
    maxicata_y_rpt = assert(io.open(maxicata_y_log, "a"))

    -- The generic mail log handler for quick reporting
    maxicata_y_mail = SCLogPath() .. "maxicata-y-mail.log"
    maxicata_y_mail_rpt = assert(io.open(maxicata_y_mail, "a"))

    -- The mail rate log reporting (for non critical alert) -raw
    maxicata_n_log = SCLogPath() .. "maxicata-n.log"
    maxicata_n_rpt = assert(io.open(maxicata_n_log, "a"))

    -- The mail rate log reporting (for non critical alert) - normalized
    maxicata_n_mail = SCLogPath() .. "maxicata-n-mail.log"
    maxicata_n_mail_rpt = assert(io.open(maxicata_n_mail, "a"))

    -- For keeping history if an IP is already reported
    --reported_ip = SCLogPath() .. "maxicata-reported-ip.log"
    --reported_ip_rpt = assert(io.open(reported_ip, "a"))

    -- The temporary path for grep (Not using this anymore)
    -- greptmp = SCLogPath() .. "grep.temp"
    -- grep_temp = assert(io.open(greptmp, "a"))

    -- The suspicious log path from Maxicata (this one contain non-critical log)
    susp_log_path = "/usr/local/maxicron/suricata/log/maxicata-SUSP_LOG_RPT.log"
    susp_log_path_rpt = assert(io.open(susp_log_path, "a"))

    -- Report count
    count = 0
    -- store non-critical alert count
    alert_count = 0
    -- non-ciritical alert rate to send email
    alert_rate = 50
end

function log() -- this will be destroy in every call

    -- CSF block status (need to put here, every call need to set false first)
    is_csf_blocked = "no" -- or "yes"

    -- Generate new value from suricata
    timestring = SCPacketTimeString()
    sid, rev, gid = SCRuleIds()
    msg = SCRuleMsg()
    class, priority = SCRuleClass()
    ip_version, src_ip, dst_ip, protocol, src_port, dst_port = SCPacketTuple()

    --Remove the decimal point for the result
    sid = math.floor(sid)
    rev = math.floor(rev)
    gid = math.floor(gid)
    priority = math.floor(priority)
    src_port = math.floor(src_port)
    dst_port = math.floor(dst_port)
    protocol = math.floor(protocol)
    ip_version = math.floor(ip_version)
    --  End remove decimal points

    if class == nil then
        class = "unknown"
    end

    if src_ip == localipv4 or src_ip == localipv6 then
        src_ip = "LOCAL_IP"
        susp_ip = dst_ip
    end

    if dst_ip == localipv4 or dst_ip == localipv6 then
        dst_ip = "LOCAL_IP"
        susp_ip = src_ip
    end

    suppress_ip = io.popen_readline("maxicata -c get-suppressed-ip-var " .. susp_ip .. " ip_address") -- readall because it can contains one or more | nope just use readline. next future readall
    -- suppress_sid = io.popen_readline("maxicata -c get-suppressed-ip-var " .. ip .. " sid") --readll because it can contains one or more | nope just use readline. next future readall --THIS ONE NEXT FEATURE
    -- suppress_time = io.popen_readline("maxicata -c get-suppressed-ip-var " .. ip .. " time") -- | nope just use readline. next future readall THIS ONE NEXT FEATURE
    -- If IP is not in the suppress list (not in suppress list = 1)
    if suppress_ip == "1" then
        logNotice("IP : " .. susp_ip .. " is not in the suppress list")
        -- After got the susp_ip above, now we call the cache function for caching IP
        set_aipdb_cached_data(susp_ip)
        -- Here after call cache from aipdb above, now we scan the suspected IP for blacklist in the background:
        -- Only call this if susp_ip is ip version 4 because ipv6 is not working for blacklist scanning
        logNotice("IP_VERSION is: " .. ip_version)
        if ip_version == 4 then
            blacklist_scan(susp_ip)
        else
            blacklist_count = 0 -- set the default value for count if ipv6
        end

        -- Now we mark this in the report if the result is cached or not
        if aipdb_cached == "new" then
            api_cached_desc = "New IP Info"
        else
            api_cached_desc = "Cached IP Info"
        end
        -- TODO this one need to transform into file called suppress at conf/suppressed_msg.lst (Maxicata will handle this)
        -- Tranform this into Rule msg whitelist
        -- Define non critical alert string but still report it as non-critical report (alert:n).
        -- This is the current non-critical Rule Message that is too noisy:
        --[[ if enable_supp_msg == true then
            if string.match(msg, "SURICATA STREAM ESTABLISHED SYNACK resend with different ACK") or
                    string.match(msg, "SURICATA STREAM ESTABLISHED SYN resend with different seq") or
                    string.match(msg, "SURICATA STREAM ESTABLISHED invalid ack") or
                    string.match(msg, "SURICATA STREAM ESTABLISHED packet out of window") or
                    string.match(msg, "SURICATA STREAM excessive retransmissions then") or
                    string.match(msg, "SURICATA STREAM Packet with invalid ack") or
                    string.match(msg, "SURICATA STREAM Packet with invalid timestamp") or
                    string.match(msg, "SURICATA STREAM pkt seen on wrong thread") or
                    string.match(msg, "SURICATA STREAM 3way handshake SYNACK resend with different ack") or
                    string.match(msg, "SURICATA STREAM 3way handshake SYN resend different seq on SYN recv") or
                    string.match(msg, "SURICATA STREAM 3way handshake wrong seq wrong ack") or
                    string.match(msg, "SURICATA STREAM TIMEWAIT ACK with wrong seq") then
                -- string.match(msg, "SURICATA Applayer Detect protocol only one direction") then -- This line just for debugging
                alert = "n"
            else
                -- if other than the above rule we need to store alert
                alert = "y"
            end

        end ]]--

        -- Search for suspicious log for this IP here:
        rpt_category = get_attack_category(susp_ip)
        logNotice("=============()()()()()()()()()()()()()()() ===============")
        logNotice("That rpot category: ")
        logNotice(rpt_category)
        logNotice("============================================================")
        susp_log_count = get_susp_log_count(susp_ip)
        logNotice("Done")
        -- This is just fo debugging: (must remove this for production)
        --alert = "n"

        -- Write log to main report --
        -- Output format: |N 1 |ALERT: n |PRIO: 3 |TIME: 10/26/2020-15:26:55.098546 |IPLAYER: {6} 68.65.121.210 {: 2082 } ->
        -- LOCAL_IP {: 443 } |GSR: [:G 1 :S 2210008 :R 2 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA STREAM 3way handshake SYN resend different seq on SYN recv
        str_rpt = "|N " .. count .. " |ALERT: " .. alert .. " |PRIO: " .. priority .. " |TIME: " .. timestring ..
                " |IPLAYER: {" .. protocol .. "} " .. src_ip .. " {: " .. src_port ..
                " } -> " .. dst_ip .. " {: " .. dst_port .. " } |GSR: [:G " .. gid .. " :S " .. sid .. " :R " .. rev ..
                " ] |CLASS: " .. class .. " |MSG: " .. msg .. "\n"
        maxicata_yn_rpt:write(str_rpt)
        maxicata_yn_rpt:flush()
        -- sleep(1) -- sleep a little bit (nope)


        -- When you call AIPDB report to report the same IP, you will be notified by the API that you can only report the same IP within 15 minutes.
        -- So calling the report also increase usage limit. So this function fixed it:
        -- Usage maxicata -c clear_ip_reported <duration_older_in_seconds
        clear_ip_reported_status = io.popen_readline("maxicata -c clear_ip_reported " .. clear_ip_cache_duration)

        if priority == 1 then
            alert_level = "Very High Risk"
        elseif priority == 2 then
            alert_level = "High Risk"
        elseif priority == 3 then
            alert_level = "Medium Risk"
        elseif priority > 3 then -- 4, 5, 7, 8, 9 above
            alert_level = "Low Risk"
        end

        -- Get the source ip count Inbound
        src_ip_count = io.popen_readline("maxicata -c get_src_ip_count " .. susp_ip .. " " .. maxicata_log)
        -- Get the destionation ip count Outbound
        dst_ip_count = io.popen_readline("maxicata -c get_dst_ip_count " .. susp_ip .. " " .. maxicata_log)
        -- Get both destination and source ip count (inbound and outbound)
        --logNotice("SRC_IP_COUNT IS (BELOW): ")
        --logNotice(src_ip_count)
        --logNotice("DST_IP_COUNT IS (BELOW): ")
        --logNotice(dst_ip_count)
        src_dst_count = tonumber(src_ip_count) + tonumber(dst_ip_count)
        -- Calculate to block
        -- Rule 1: If Priority Level is: Very High Risk (1) , High Risk (2), Medium Risk (3) AND abuse_score is >=80, then block.
        -- Here they tell me what is right and I block that IP (I don't report)
        if (tonumber(priority) <= 3 and tonumber(aipdb_abuse_score) >= 80) then
            csf_execute_block("AIPDB Rule")
            score_status = "bad"
        end
        -- Rule 1: If priority >=3 AND blacklist count >=7  AND susp_log_count >=1, then need to report and block | 7 is the best number I think
        -- Rule 2: If priority = 1, then need to report and block (Very High Risk Rule by Suricata)
        -- Rule 3: If priority = 2, then need to report and block (High Risk Rule by Suricata)
        -- Here I tell them what is right, I report and block that IP so they can decide to block too. (now they also need to tell me abit about the score)
        if (tonumber(priority) >= 3 and tonumber(blacklist_count) >= 7 and tonumber(susp_log_count) >= 1) or (tonumber(aipdb_abuse_score) >= 65 and tonumber(blacklist_count) >= 5) or (tonumber(priority) == 1) or (tonumber(priority) == 2) then
            csf_execute_block("Maxicata Rule")
            logNotice("The AIPDB report condition is triggered +!+!+!+")
            -- This one must follow AIPDB rule here for suricata: https://www.abuseipdb.com/suricata
            if string.match(class, "web application") then
                surCategory = "21" -- Web App Attack
            elseif string.match(class, "user") or string.match(class, "User") or string.match(class, "administrator") or string.match(class, "Administrator") then
                surCategory = "18" -- Bruteforce
            elseif string.match(class, "suspicious username") or string.match(class, "default username") then
                surCategory = "18,22" -- Bruteforce, SSH
            elseif (string.match(class, "rpc") or string.match(class, "Network scan") or string.match(class, "Information Leak")) then
                surCategory = "14" -- Port scan
            elseif string.match(class, "Denial of Service") then
                surCategory = "4" -- DDOS Attack
            elseif rpt_category ~= "0" then
                surCategory = rpt_category -- Use category from the log suspicious from Maxicata if it exist
            else
                surCategory = "15" -- If not above just use the default category, Hacking.
            end

            if string.match(SCRuleMsg(), "SQL INJECTION") then
                surCategory = surCategory .. ",16" -- Above category + SQL Injection
            end

            -- Setting up comment:
            surComment = "|TIME: " .. timestring .. " |ALERT_LEVEL: " .. alert_level .. " (" .. priority .. "/5) |RBL_BLACKLISTED_COUNT: " ..
                    blacklist_count .. " |SUSP_LOG_COUNT: " .. susp_log_count .. " |ABUSE_CATEGORY: " .. surCategory .. " |CRITICAL_REPORT: " .. alert .. " |IN(" .. src_ip_count .. ") + OUT(" .. dst_ip_count .. ")=" .. src_dst_count .. " times" ..
                    " |CURRENT_STREAM_RECORD: " .. src_ip .. ":" .. src_port ..
                    " => " .. dst_ip .. ":" .. dst_port .. " [class]: " .. class .. " [activity]: " .. msg

            -- if ip already reported, don't report again (until manually clear the reported ip cache) .. setup cache for reported
            -- we want to also reduce this API call
            -- Now call the report
            -- aipdb_enable_report = false -- temporariliy disable report (for debugging) haven't finish here

            if aipdb_enable_report == true then
                -- Before report, we need to check the reported_ip file if timestamp expired, then we remove it
                err_status = io.popen_readall("maxicata -c report_aipdb '" .. susp_ip .. "' '" .. surCategory .. "' '" .. surComment .. "'")
                aipdb_report_desc = err_status
            else
                aipdb_report_desc = "Warning, report was set but didn't run. Something is wrong"
            end
            score_status = "bad"
        else
            score_status = "unknown"
            aipdb_report_desc = "This IP doesn't have enough bad score to be reported at AIPDB"
            alert_level_action = "Unable to determine more reputation score for this IP. Please audit this IP manually"
        end

        if (src_dst_count == 0 or src_dst_count == 1) then -- This means that it has history of suspicious activity
            alert_cache_subject = "New"
        else
            alert_cache_subject = "Existing"
        end

        -- Here we can modify the alert based on specific condition to non-critical alert to reduce report. For example Search Engine Spider.
        -- If score is bad and csf already block then we don't need to alert, we put it in non-critical alert
        if score_status ~= "ok" or is_csf_blocked ~= "yes" then
            alert = "y"
            -- else -- ok
            -- alert = "y"
        end

        if aipdb_iswhitelisted == "true" then -- if aipdb reported this IP is whitelist, then we should put it as non-critical
            alert ="n"
        end

        -- IF CSF already block this IP, put it on alert = "n"

        -- Modify the alert (y/n) before sending to log based on usage type from AIPDB:
        --if aipdb_usage_type == "Search Engine Spider" then -- Make Search Engine Spider as non-critical alert IP (if this data is available) SES is good for my site SEO
        --    logNotice("AIPDB_USAGE_TYPE IS: ")
        --    logNotice(aipdb_usage_type)
        --    alert = "n"
        -- end
        -- TODO, create a whitelist db to whitelist IP where if found in this DB, and type is temp: we just assign alert = "n" (because we still need this data)
        -- TODO continue if type is perm (permanent) we don't record this.
        -- |whitelisted_ip: 1.1.1.1 |type: temp |time: 23232132 (This one is the suppress whitelist) maxicata -c suppress-temp <ip> / maxicata -c suppress-perm <ip> / maxicata suppress-n <ip>
        -- This is just fo debugging: (must remove this for production)
        --alert = "n"
        cmd_man_blockstr = "maxicata -c deny " .. susp_ip .. " 'Suricata_CSF_Manual_Block | GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_code .. "| Abuse_Percentage:" .. aipdb_abuse_score
                .. "% | Alert_Level: " .. alert_level .. " |RBL_Blacklisted_Count: " .. blacklist_count .. "| Inbound_Outbound: " .. src_dst_count .. "'"

        suppress_ip_cmd = "maxicata -sip " .. susp_ip .. " " .. sid .. " '|GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_name .. "/" .. aipdb_country_code .. " |Abuse_Percentage:" .. aipdb_abuse_score .. "%" .. "'"

        if (alert == "y") then
            -- Only call API if alert is y -- good for API limit usage
            -- Call the AIPDB data - dont run if previosly already call the same ip to prevent API limit usage
            y_rpt_str = "|n " .. count .. " |time: " .. timestring .. " |critical_rpt: y |prio: " .. priority ..
                    " |score: " .. score_status .. " |rbl_count: " .. blacklist_count .. " |susp_log_count: " .. susp_log_count ..
                    " |is_csf_blocked: " .. is_csf_blocked .. " |aipdb_iswhitelisted: " .. aipdb_iswhitelisted ..
                    " |aipdb_score: " .. aipdb_abuse_score .. " |ip_layer: {" .. protocol .. "} " .. src_ip .. " {: " .. src_port ..
                    " } -> " .. dst_ip .. " {: " .. dst_port .. " } |GSR: [:G " .. gid .. " :S " .. sid .. " :R " .. rev ..
                    " ] |class: " .. class .. " |msg: " .. msg .. "\n"
            maxicata_y_rpt:write(y_rpt_str)
            maxicata_y_rpt:write("\n")
            maxicata_y_rpt:flush()

            maxicata_y_mail_rpt:write("Warning, new suspicious IP from [" .. susp_ip .. "/" .. aipdb_country_name .. "]" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "AbuseIPDB Security Info [" .. api_cached_desc .. "]" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Abuse Confidence Score: " .. aipdb_abuse_score .. "%" .. "\n"
                    .. "IP: " .. aipdb_ip .. "\n"
                    .. "is IP whitelisted: " .. aipdb_iswhitelisted .. "\n"
                    .. "ISP: " .. aipdb_isp .. "\n"
                    .. "Usage Type: " .. aipdb_usage_type .. "\n"
                    .. "Domain Name: " .. aipdb_domain .. "\n"
                    .. "Country Name/Code: " .. aipdb_country_name .. "/" .. aipdb_country_code .. "\n"
                    .. "AIPDB Report Information: " .. "\n"
                    .. "*---*" .. "\n"
                    .. "1) This IP address has been reported a total of " .. aipdb_total_report .. " time(s) from " .. aipdb_distinct_report
                    .. " distinct sources. " .. susp_ip .. " most recent report was [" .. aipdb_last_report .. "]. More info at: https://www.abuseipdb.com/check/" .. susp_ip .. "\n"
                    .. "2) " .. aipdb_report_desc .. "\n"
                    .. "3) " .. clear_ip_reported_status .. "\n"
                    .. "*---*" .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Suricata Security Info:" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Risk Priority Level: " .. alert_level .. " (" .. priority .. "/5)" .. "\n"
                    .. "Source/Inbound(" .. src_ip_count .. ") + Destination/Outbound(" .. dst_ip_count .. ") = " .. src_dst_count .. " record(s)" .. "\n"
                    .. "Blacklist SPAM Count: " .. blacklist_status .. " (" .. blacklist_count .. ")" .. "\n"
                    .. "Suspicious Log Count: " .. susp_log_count .. "\n"
                    .. "AIPDB Attack Category Code: " .. surCategory .. " [More info at: http://abuseipdb.com/categories]" .. "\n"
                    .. "Info & Recommend Action: " .. alert_level_action .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Your Action: " .. "\n"
                    .. "--------------------- " .. "\n" --
                    .. "1) Block this IP using CSF: " .. "\n"
                    .. cmd_man_blockstr .. "\n"
                    .. "\n"
                    .. "2) Few commands to suppress this IP: " .. "\n"
                    .. "a) Suppress this IP from reporting in suricata:" .. "\n"
                    .. suppress_ip_cmd .. "\n"
                    .. "\n"
                    .. "b) Suppress this type of suspicious message:" .. "\n"
                    .. "Coming soon"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Suspicious IP web references:" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "1) https://www.abuseipdb.com/check/" .. susp_ip .. "\n"
                    .. "2) https://infobyip.com/ip-" .. susp_ip .. ".html" .. "\n"
                    .. "3) https://stopforumspam.com/ipcheck/" .. susp_ip .. "\n"
                    .. "4) https://www.spamhaus.org/query/ip/" .. susp_ip .. "\n"
                    .. "5) https://talosintelligence.com/reputation_center/lookup?search=" .. susp_ip .. "\n"
                    .. "6) http://www.borderware.com/lookup.php?ip=" .. susp_ip .. "\n"
                    .. "7) https://www.ipqualityscore.com/ip-reputation-check/lookup/" .. susp_ip .. "\n"
                    .. "8) http://" .. susp_ip .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Current suspicious activity from [" .. susp_ip .. "/" .. aipdb_country_name .. "]:" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. str_rpt .. "\n"
                    .. "---" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "\n"
                    .. "-----------------" .. "\n"
                    .. "History of suspicious activities from [" .. susp_ip .. "/" .. aipdb_country_name .. "] with [alert=yes]:" .. "\n"
                    .. "-----------------" .. "\n")
            maxicata_y_mail_rpt:flush()
            io.popen_x("grep -F '" .. susp_ip .. "' '" .. maxicata_y_log .. "' >> '" .. maxicata_y_mail .. "'") --TODO transform this into maxicata for accurate reading
            maxicata_y_mail_rpt:write("---" .. "\n"
                    .. "\n"
                    .. "-----------------" .. "\n"
                    .. "History of suspicious activities from [" .. susp_ip .. "/" .. aipdb_country_name .. "] with [alert=no]:" .. "\n"
                    .. "--------------------- " .. "\n")
            maxicata_y_mail_rpt:flush()
            io.popen_x("grep -F '" .. susp_ip .. "' '" .. maxicata_n_log .. "' >> '" .. maxicata_y_mail .. "'") --TODO transform this into maxicata for accurate reading
            maxicata_y_mail_rpt:write("---" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Other suspicious log files from this IP: [" .. susp_ip .. "/" .. aipdb_country_name .. "]:" .. "\n"
                    .. "--------------------- " .. "\n")
            maxicata_y_mail_rpt:flush()
            io.popen_x("cat " .. susp_log_path .. " >> " .. maxicata_y_mail)
            maxicata_y_mail_rpt:write("---" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "\n")
            maxicata_y_mail_rpt:flush()
            cmdstr = "mail -s '[Suricata | ALERT: " .. alert_level .. " (" .. src_dst_count .. ") ]: " .. alert_cache_subject .. " Suspicious IP from [" .. susp_ip .. "/" .. aipdb_country_name .. "] @ "
                    .. hostname .. "' '" .. email .. "' < " .. maxicata_y_mail
            io.popen_x(cmdstr)
            io.open(maxicata_y_mail, "w"):close() -- clear the log file
        else -- if alert = n (non critical)
            alert_count = alert_count + 1
            -- Just show list of non critical alert from this file, but we need to store this first at above inside maxicata_n_log... so dont use maxicata_log -- done
            -- insert the last line into the non critical-report file to be used later
            n_rpt_str = "|n " .. count .. " |time: " .. timestring .. " |critical_rpt: n |prio: " .. priority ..
                    " |score: " .. score_status .. " |rbl_count: " .. blacklist_count .. " |susp_log_count: " .. susp_log_count ..
                    " |is_csf_blocked: " .. is_csf_blocked .. " |aipdb_iswhitelisted: " .. aipdb_iswhitelisted ..
                    " |aipdb_score: " .. aipdb_abuse_score .. " |ip_layer: {" .. protocol .. "} " .. src_ip .. " {: " .. src_port ..
                    " } -> " .. dst_ip .. " {: " .. dst_port .. " } |GSR: [:G " .. gid .. " :S " .. sid .. " :R " .. rev ..
                    " ] |class: " .. class .. " |msg: " .. msg .. "\n"
            maxicata_n_rpt:write(n_rpt_str)
            maxicata_n_rpt:write("\n")
            maxicata_n_rpt:flush()

            if alert_count == alert_rate then -- it's time to display the noncritical report if reached this count
                -- Display each IP and also count, so it's easier to see if IP is distinct. (done)
                alert_count = 0 -- reset back to 0 to re-count
                -- Calculate the non-critical ip using maxicata (bash)
                maxicata_n_mail_rpt:write("Suricata has collected non-critical activites with detection rate of " .. alert_rate .. " times" .. "\n"
                        .. "---------" .. "\n"
                        .. "\n")
                maxicata_n_mail_rpt:flush()
                -- current correct field: <src_ip_fld> <dst_ip_fld> <last_record_sid> 21 26 34
                logNotice("Critical log report has reached limit to send notification. Calculating non-critical IP abuse information ...")
                io.popen_x("maxicata -c calc-nc-ip " .. maxicata_n_log .. " 21 26 34 >> " .. maxicata_n_mail)
                maxicata_n_mail_rpt:write("\n"
                        .. "---------" .. "\n")
                maxicata_n_mail_rpt:flush()
                -- Send email from the above file after it has reached threshold of alert_rate value
                io.popen_x("mail -s '[Sucirata | Rate: " .. alert_rate .. "]: Non-Critical IP Report From Suricata @ " .. hostname .. "' '" .. email .. "' < " .. maxicata_n_mail)
                -- clear log file of maxicata_n_mail because we don't need to keep this as this file is just a temporary file to handle mail.
                io.open(maxicata_n_mail, "w"):close()
            end
        end
        -- count total record each time suricata detects new suspicious activity (both critical and non-critical)
        count = count + 1
    else
        logNotice("IP : " .. susp_ip .. " is in suppress list. Ignore this IP in report")
    end
end

function deinit()
    -- Close all report files
    logNotice("Maxicata Reports Logged: " .. count);
    io.close(maxicata_rpt)
    io.close(maxicata_yn_rpt)
    io.close(maxicata_y_rpt)
    io.close(maxicata_y_mail_rpt)
    io.close(maxicata_n_rpt)
    io.close(maxicata_n_mail_rpt)
    --io.close(grep_temp)
    io.close(susp_log_path_rpt)
end