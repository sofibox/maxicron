Sample output:
```
[maxiaide]: OK, no duplicated process detected
-------
[maxiaide]: Running mode: [cron]
[maxiaide]: Ok, found previous database to compare: [/usr/local/maxicron/aide/db/aide.db.gz]
[maxiaide]: AIDE is checking file integrity ... (this may take some time):
-------
---= CHECK AND UPDATE START =---

Start timestamp: 2020-10-09 02:20:02 +0800 (AIDE 0.16)
AIDE found differences between database and filesystem!!
New AIDE database written to /usr/local/maxicron/aide/db/aide.db.new.gz

Summary:
  Total number of entries:    209104
  Added entries:        1
  Removed entries:        0
  Changed entries:        5

---------------------------------------------------
Added entries:
---------------------------------------------------

f++++++++++++++++: /usr/local/csf/tpl/modsecipdbcheck.txt

---------------------------------------------------
Changed entries:
---------------------------------------------------

f   ...     C..  : /usr/local/csf/tpl/alert.txt
f   ...     C..  : /usr/local/lynis/CHANGELOG.md
f   ...     C..  : /usr/local/lynis/include/osdetection
f   ...     C..  : /usr/local/lynis/include/tests_crypto
f   ...     C..  : /usr/local/lynis/lynis

---------------------------------------------------
Detailed information about changes:
---------------------------------------------------

File: /usr/local/csf/tpl/alert.txt
  SHA256   : YIszO26OSroFYdhjLe2PLBj2N2etmmpK | IceeH8YjTbQN9KXWlxGCKnA3xiWcOr4V
             tkOUYtYhXng=                     | 0qqdz9AftQ0=

File: /usr/local/lynis/CHANGELOG.md
  SHA256   : wMBUs0e26Uor0bPWzxVA23HuoN/xFQqW | /7DlDo72nPBDYWWj5NuOKX17OwD+C+dR
             p3+jPFqW8PU=                     | wVNBMNlNXVI=

File: /usr/local/lynis/include/osdetection
  SHA256   : 5BJx5vMvxd1SkmfEVmoxF0Lh2M5+lxRU | KcjUWrfK2SivRidj2R4/SVhMkgFZuvAY
             WHo1S8dwKmI=                     | p9b7bf/lerE=

File: /usr/local/lynis/include/tests_crypto
  SHA256   : ACj3CAcfTFoWe3e9gWog0rmu31o0uyTd | sD7+zEtrQ6iLM+eewg2xmNEeclBd8yxe
             g42K8EgSZ9U=                     | AfbWDUvBxUI=

File: /usr/local/lynis/lynis
  SHA256   : v4mgWq4FoOrZYngF35YVu4+uBSmxuGDA | 5QbV3rHmREs8aoclnhTtR8wac4QlLpXE
             4HlAujWEg60=                     | 0pEky9sw8/g=


---------------------------------------------------
The attributes of the (uncompressed) database(s):
---------------------------------------------------

/usr/local/maxicron/aide/db/aide.db.gz
  MD5      : A/xg32bxYNhRn73SrwnR7g==
  SHA1     : 5wQMNjonx7/wgwFt2EeKf0RKRjE=
  RMD160   : naoZbnj0bucvk98qN79ZdsjaB00=
  TIGER    : KkQqlozjzoa8i+AlZ8+2AixaJ1lZPE9O
  SHA256   : 2lbFtDLNQbBLi6vEtoSpRnQnFKkz061E
             aVqev1TgDYI=
  SHA512   : ChVx2mzxexXdDdnlyPKRWaU1N9uzmacV
             Mb2drGlqG3qiVuLCqZ6HYwOhIj1RTwGP
             O63zlYAVYELobrNpDpDP8Q==

/usr/local/maxicron/aide/db/aide.db.new.gz
  MD5      : ddS/hKWd9vQTgbznHCdA+Q==
  SHA1     : 2HKrvUNCQKCvQGL/bmiQUHETHbE=
  RMD160   : YJbyKvBY9et6eB5BbpIv5k5Jbqk=
  TIGER    : cqXA4tMHubAh4q7LcFY6Wd9iDolT3boe
  SHA256   : 6LEjkV4FUNOcYzlhn1QcSw3EYbD4D6t1
             W6aai/jLtgE=
  SHA512   : qG6zMUrQ4AtqMqyKVbvMyP6e/FBqQfZ6
             Vvt7kRer2xbZU4uxSKH/BI33/pBMJEmi
             cmwKRb3nKw2WyI7saiOeRQ==


End timestamp: 2020-10-09 02:22:31 +0800 (run time: 2m 29s)
[aide]: AIDE return_code after update: [5]
[aide]: Warning, 1 new and 5 changed files detected
[aide]: AIDE database of [/usr/local/maxicron/aide/db/aide.db.new.gz] was renamed as [/usr/local/maxicron/aide/db/aide.db.gz]
[aide]: AIDE has finished checking

---= CHECK AND UPDATE END =---

=============================================
================ SUMMARY ====================
=============================================
[aide]: Scan completed successfully
[aide]: Scan status: WARNING | 1 new and 5 changed files detected
[aide]: Log file is located at /usr/local/maxicron/aide/log/aide-log-09-10-2020-12920.log
[aide]: Total runtime: [0:00:02:29.6806]
=============================================
[maxiaide | info]: Preparing to upload log file into [onedrive] ...
[maxiaide | info]: Decrypting rclone config file ...
[maxiaide | info]: OK, rclone config file decrypted successfully
[maxiaide | info]: Creating new backup directory in [onedrive] as [Earthbox/maxicron_logs/AIDE/09-10-2020/] ...
[maxiaide | info]: OK, new report folder [Earthbox/maxicron_logs/AIDE/09-10-2020/] created at [onedrive]
[maxiaide | info]: Success, report file of [/usr/local/maxicron/aide/log/aide-log-09-10-2020-12920.log] has been successfully uploaded into [onedrive]

```