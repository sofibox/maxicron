-- Lua sample API usage in Centos 8
-- A sample usage of AIPDB API
-- Author - Arafat Ali | Email: webmaster@sofibox.com

ip = "118.25.6.39" -- Sample IP
api_key_aipdb = "dbc4cujJDKSLDS5c835caMJdsajkdld04dc597bssaadf61ac8edfd39d16f43" -- Sample API (this is dummy)

local cmdraw = "curl -G https://api.abuseipdb.com/api/v2/check --data-urlencode 'ipAddress=" .. ip .. "' -d maxAgeInDays=90 -d verbose -H 'Key: " .. api_key_aipdb .. "' -H 'Accept: application/json' | jq '.'"

-- Read the json
local jsonp = io.popen(cmdraw)
local aipdb_data = jsonp:read("*a")
jsonp:close()


-- Populate any variable you want

-- For Confident score
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.abuseConfidenceScore'")
aipdb_abuse_score = handle:read("*line")

-- For Country Code
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.countryCode'")
aipdb_country_code = handle:read("*line")

-- For Country Name
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.countryName'")
aipdb_country_name = handle:read("*line")

-- For Usage Type
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.usageType'")
aipdb_usage_type = handle:read("*line")

-- For ISP name
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.isp'")
aipdb_isp = handle:read("*line")

-- For Domain Name
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.domain'")
aipdb_domain = handle:read("*line")

-- For Total Reports
handle = io.popen("echo '" .. aipdb_data .. "' | jq -r '.data.totalReports'")
aipdb_total_report = handle:read("*line")


handle:close()

print(aipdb_abuse_score)
print(aipdb_country_code)
print(aipdb_country_name)
print(aipdb_usage_type)
print(aipdb_isp)
print(aipdb_domain)
print(aipdb_total_report)

Output:

23
CN
China
Data Center/Web Hosting/Transit
Tencent Cloud Computing (Beijing) Co. Ltd
tencent.com
7