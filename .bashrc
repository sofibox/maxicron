# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

export GPG_TTY=$(tty)
nohup bash maxicata init </dev/null >/dev/null 2>&1 &