-- This is a simple example script to show what you can do with lua output scripts.
-- It prints logs similar to the ones produced by the builtin fast.log output
-- faciltiy to stdout, hence its name.

-- In the init() function we tell suricata, that we want the log function to be
-- called for every packet that produces an alert (see needs variable)

-- Then in the log() function we get various informations about this packet via
-- SCRuleMsg() and all the other API functions and print them to stdout with print()

-- To learn more about all the API functions suricata provides for your lua scripts
-- and the lua output extension in general see:
-- http://suricata.readthedocs.io/en/latest/output/lua-output.html

function init()
    local needs     = {}
    needs["type"]   = "packet"
    needs["filter"] = "alerts"
    return needs
end

function setup()
    -- The main Maxicata lua log
    name = "maxicata.log"
    filename = SCLogPath() .. "/" .. name
    file = assert(io.open(filename, "a"))
    SCLogInfo("Maxicata Log Filename " .. filename)
    alert_count = 0
end

function log()
    localipv4 = "yourlocalipv4"
    localipv6 = "yourlocalipv6"
    timestring      = SCPacketTimeString()
    sid, rev, gid   = SCRuleIds()
    msg             = SCRuleMsg()
    class, priority = SCRuleClass()
    alert = "no"
    ip_version, src_ip, dst_ip, protocol, src_port, dst_port = SCPacketTuple()

    if class == nil then
        class = "unknown"
    end
    
    if src_ip == localipv4 or src_ip == localipv6 then
       src_ip = "sofibox_local_ip"
    end
    
    if dst_ip == localipv4 or dst_ip == localipv6 then
       dst_ip = "sofibox_local_ip"
    end

    if string.match(msg, "SURICATA STREAM") or string.match(msg, "SURICATA TLS invalid") then
       alert = "alert:no"
    else
       alert = "alert:yes"
    end

    -- file:write(timestring .. "  [**] [" .. gid .. ":" .. sid .. ":" .. rev .. "] " ..
    --       msg .. " [**] [Classification: " .. class .. "] [Priority: " ..
    --       priority .. "] {" .. protocol .. "} " ..
    --       src_ip .. ":" .. src_port .. " -> " .. dst_ip .. ":" .. dst_port .. "\n")
    
    file:write(alert_count .. "] " .. alert .. " " .. timestring .. " "  .. src_ip .. " " .. dst_ip .. " " .. ip_version .. " " .. msg .. "\n")
    
    file:flush()

    alert_count = alert_count + 1;
end

function deinit()
    SCLogInfo ("Maxicata Reports Logged: " .. alert_count);
    file.close(file)
end
